const { request, response } = require('express');
const db = require('../dbConnection');

const amenitiesList = (request, response) => {
    const { keyword } = request.body;
    var sql = `SELECT * FROM amenities where status = 1`;
    if (keyword) {
        sql += ` AND name LIKE N'%${keyword}%'`;
    }
    sql += ` ORDER BY priority_order`;
    db.query(sql,  (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const addAmenity = (request, response) => {
    const { name, priority_order } = request.body;
    if (!name) {
        response('Amenity Name is empty');
    } else {
        db.query("INSERT INTO amenities (name, priority_order) VALUES (?, ?)", [name, priority_order], (error, result) => {
            if (error) {
                response('Database connection error');
            } else {
                response(null, result.insertId);
            }
        });
    }
}

const updateAmenity = (request, response) => {
    const { id, name, priority_order } = request.body;
    if (!id) {
        response('ID Missing');
    } else if (!name) {
        response('Amenity Name is empty');
    } else {
        db.query('UPDATE amenities SET name = ?, priority_order = ? WHERE id = ?', [name, priority_order, id], (error, result) => {
            if (error) {
                response('Database connection error');
            } else {
                response(null, result);
            }
        });
    }
}

const changeAmenityStatus = (request, response) => {
    const id = parseInt(request.params.id);
    const status = parseInt(request.params.status);
    db.query('UPDATE amenities SET status = ? WHERE id = ?', [status, id], (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const changeAmenityPriorityOrder = (request, response) => {
    const id = parseInt(request.params.id);
    const priority_order = parseInt(request.params.priority_order);
    db.query('UPDATE amenities SET priority_order = ? WHERE id = ?', [priority_order, id], (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const deleteAmenity = (request, response) => {
    const id = parseInt(request.params.id);
    db.query('DELETE FROM amenities WHERE id = ?', [id], (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

module.exports = {
    amenitiesList,
    addAmenity,
    updateAmenity,
    changeAmenityStatus,
    changeAmenityPriorityOrder,
    deleteAmenity
}