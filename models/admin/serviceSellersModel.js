const { request, response } = require('express');
const db = require('../dbConnection');
const fs = require('fs');

const serviceSellersList = (request, response) => {
    const { keyword } = request.body;
    var sql = `SELECT u.id, u.name, u.email, u.mobile_number, u.status, s.business_experience, s.tags, s.about, s.images, s.opening_time, s.closing_time, GROUP_CONCAT(services.name) AS services FROM users AS u LEFT JOIN service_sellers AS s ON u.id=s.user_id LEFT JOIN services ON FIND_IN_SET(services.id, s.services_offered) WHERE`;
    if (keyword) {
        sql += ` u.name LIKE N'%${keyword}%' OR u.email LIKE N'%${keyword}%' OR u.mobile_number LIKE N'%${keyword}%' AND`;
    }
    sql += ` u.is_service_seller = 1 GROUP BY u.id`;
    db.query(sql,  (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const getServiceSellerDetails = (request, response) => {
    const id = parseInt(request.params.id);
    db.query("SELECT u.id, u.name, u.email, u.mobile_number, u.status, s.business_experience, s.tags, s.about, s.images, s.opening_time, s.closing_time, GROUP_CONCAT(services.name) AS services FROM users AS u LEFT JOIN service_sellers AS s ON u.id=s.user_id LEFT JOIN services ON FIND_IN_SET(services.id, s.services_offered) WHERE u.id = ?", [id], (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const changeServiceSellerStatus = (request, response) => {
    const id = parseInt(request.params.id);
    const status = parseInt(request.params.status);
    db.query('UPDATE users SET status = ? WHERE id = ?', [status, id], (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const deleteServiceSeller = (request, response) => {
    const id = parseInt(request.params.id);
    db.query('SELECT images FROM service_sellers WHERE user_id = ?', [id], (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            if (result[0].images) {
                var array = result[0].images.split(', ');
                array.forEach(element => {
                    fs.unlink(element, (err) => {
                        if (err) {
                            response('Error deleting image');
                        }
                    });
                });
                
                db.query('DELETE FROM users WHERE id = ?', [id], (error, result) => {
                    if (error) {
                        response('Database connection error');
                    } else {
                        response(null, result);
                    }
                });
            }
        }
    });
}

module.exports = {
    serviceSellersList,
    getServiceSellerDetails,
    changeServiceSellerStatus,
    deleteServiceSeller
}