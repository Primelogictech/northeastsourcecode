const { request, response } = require('express');
const amenitiesModel = require('../../models/admin/amenitiesModel');

const amenitiesList = (request, response) => {
    amenitiesModel.amenitiesList(request, (err, result) => {
        if (err) {
            response.status(400).send('Something went wrong');
        } else {
            response.status(200).json({data: result});
        }
    });
}

const addAmenity = (request, response) => {
    amenitiesModel.addAmenity(request, (err, result) => {
        if (err) {
            response.status(400).send('Something went wrong');
        } else {
            response.status(200).json({message: 'Amenity Added Successfully', amenityId: result});
        }
    });
}

const updateAmenity = (request, response) => {
    amenitiesModel.updateAmenity(request, (err, result) => {
        if (err) {
            response.status(400).send('Something went wrong');
        } else {
            if (!result.affectedRows) {
                response.status(200).json({message: 'No Amenity Found'});
            } else {
                response.status(200).json({message: 'Amenity Updated Successfully'});
            }
        }
    });
}

const changeAmenityStatus = (request, response) => {
    amenitiesModel.changeAmenityStatus(request, (err, result) => {
        if (err) {
            response.status(400).send('Something went wrong');
        } else {
            if (!result.affectedRows) {
                response.status(200).json({message: 'No Amenity Found'});
            } else {
                response.status(200).json({message: 'Status Changed Successfully'});
            }
        }
    });
}

const changeAmenityPriorityOrder = (request, response) => {
    amenitiesModel.changeAmenityPriorityOrder(request, (err, result) => {
        if (err) {
            response.status(400).send('Something went wrong');
        } else {
            if (!result.affectedRows) {
                response.status(200).json({message: 'No Amenity Found'});
            } else {
                response.status(200).json({message: 'Priority Order Changed Successfully'});
            }
        }
    });
}

const deleteAmenity = (request, response) => {
    amenitiesModel.deleteAmenity(request, (err, result) => {
        if (err) {
            response.status(400).send('Something went wrong');
        } else {
            if (!result.affectedRows) {
                response.status(200).json({message: 'No Amenity Found'});
            } else {
                response.status(200).json({message: 'Amenity Deleted Successfully'});
            }
        }
    });
}

module.exports = {
    amenitiesList,
    addAmenity,
    updateAmenity,
    changeAmenityStatus,
    changeAmenityPriorityOrder,
    deleteAmenity
}