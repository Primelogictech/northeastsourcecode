const { request, response } = require('express');
const pagesModel = require('../../models/admin/pagesModel');
const generateToken = require('../generateToken');

const verifyLogin = (request, response) => {
    pagesModel.verifyLogin(request, (err, result) => {
        if (err) {
            response.render('admin/login', { error: err });
        } else {
            generateToken(response, result[0].id, result[0].name);
            response.redirect('index');
        }
    });
}

const dashboard = (request, response) => {
    pagesModel.countUsers(request, (err, result) => {
        if (err) {
            response.render('admin/index', { error: err });
        } else {
            response.render('admin/index', { data: result });
        }
    });
}

const manageUsers = (request, response) => {
    pagesModel.manageUsers(request, (err, result) => {
        if (err) {
            response.render('admin/manageUsers', { error: err });
        } else {
            response.render('admin/manageUsers', { data: result });
        }
    });
}

const manageSellers = (request, response) => {
    pagesModel.manageSellers(request, (err, result) => {
        if (err) {
            response.render('admin/manageSellers', { error: err });
        } else {
            response.render('admin/manageSellers', { data: result });
        }
    });
}

const viewSellerProperties = (request, response) => {
    pagesModel.viewSellerProperties(request, (err, result) => {
        if (err) {
            response.render('admin/viewSellerProperties', { error: err });
        } else {
            response.render('admin/viewSellerProperties', { data: result });
        }
    });
}

const viewPropertyDetails = (request, response) => {
    pagesModel.viewPropertyDetails(request, (err, result) => {
        if (err) {
            response.render('admin/propertyDetails', { error: err });
        } else {
            response.render('admin/propertyDetails', { data: result });
        }
    });
}

const manageProperties = (request, response) => {
    pagesModel.manageProperties(request, (err, result) => {
        if (err) {
            response.render('admin/manageProperties', { error: err });
        } else {
            response.render('admin/manageProperties', { data: result, keyword: request.body.keyword });
        }
    });
}

const manageServiceSellers = (request, response) => {
    pagesModel.manageServiceSellers(request, (err, result) => {
        if (err) {
            response.render('admin/manageServiceSellers', { error: err });
        } else {
            response.render('admin/manageServiceSellers', { data: result, keyword: request.body.keyword });
        }
    });
}

const viewPropertyTypes = (request, response) => {
    pagesModel.viewPropertyTypes(request, (err, result) => {
        if (err) {
            response.render('admin/viewPropertyTypes', { error: err });
        } else {
            response.render('admin/viewPropertyTypes', { data: result });
        }
    });
}

const addPropertyType = (request, response) => {
    const id = parseInt(request.params.id);
    if (id) {
        pagesModel.addPropertyType(request, (err, result) => {
            if (err) {
                response.render('admin/addPropertyType', { error: err });
            } else {
                response.render('admin/addPropertyType', { data: result });
            }
        });
    } else {
        response.render('admin/addPropertyType');
    }
}

const uploadPropertyType = (request, response) => {
    pagesModel.uploadPropertyType(request, (err, result) => {
        if (err) {
            response.render('admin/addPropertyType', { error: err });
        } else {
            response.render('admin/addPropertyType', { success: 'Priority Type added/updated successfully' });
        }
    });
}

const viewPropertySubTypes = (request, response) => {
    pagesModel.viewPropertyTypes(request, (err, pt) => {
        if (err) {
            response.render('admin/viewPropertySubTypes', { error: err });
        } else {
            pagesModel.viewPropertySubTypes(request, (err, result) => {
                if (err) {
                    response.render('admin/viewPropertySubTypes', { error: err });
                } else {
                    response.render('admin/viewPropertySubTypes', { data: result, property_types: pt  });
                }
            });
        }
    });
}

const addPropertySubType = (request, response) => {
    const id = parseInt(request.params.id);
    if (id) {
        pagesModel.viewPropertyTypes(request, (err, pt) => {
            if (err) {
                response.render('admin/viewPropertySubTypes', { error: err });
            } else {
                pagesModel.addPropertySubType(request, (err, result) => {
                    if (err) {
                        response.render('admin/addPropertySubType', { error: err });
                    } else {
                        response.render('admin/addPropertySubType', { data: result, property_types: pt });
                    }
                });
            }
        });
    } else {
        pagesModel.viewPropertyTypes(request, (err, pt) => {
            if (err) {
                response.render('admin/viewPropertySubTypes', { error: err });
            } else {
                response.render('admin/addPropertySubType', { property_types: pt });
            }
        });
    }
}

const uploadPropertySubType = (request, response) => {
    pagesModel.uploadPropertySubType(request, (err, result) => {
        if (err) {
            response.render('admin/addPropertySubType', { error: err });
        } else {
            response.render('admin/addPropertySubType', { success: 'Priority Sub Type added/updated successfully' });
        }
    });
}

const viewServices = (request, response) => {
    pagesModel.viewServices(request, (err, result) => {
        if (err) {
            response.render('admin/viewServices', { error: err });
        } else {
            response.render('admin/viewServices', { data: result, keyword: request.body.keyword });
        }
    });
}

const addService = (request, response) => {
    const id = parseInt(request.params.id);
    if (id) {
        pagesModel.addService(request, (err, result) => {
            if (err) {
                response.render('admin/addService', { error: err });
            } else {
                response.render('admin/addService', { data: result });
            }
        });
    } else {
        response.render('admin/addService');
    }
}

const uploadService = (request, response) => {
    pagesModel.uploadService(request, (err, result) => {
        if (err) {
            response.render('admin/addService', { error: err });
        } else {
            response.render('admin/addService', { success: result });
        }
    });
}

const viewAmenities = (request, response) => {
    pagesModel.viewAmenities(request, (err, result) => {
        if (err) {
            response.render('admin/viewAmenities', { error: err });
        } else {
            response.render('admin/viewAmenities', { data: result });
        }
    });
}

const addAmenity = (request, response) => {
    const id = parseInt(request.params.id);
    if (id) {
        pagesModel.addAmenity(request, (err, result) => {
            if (err) {
                response.render('admin/addAmenity', { error: err });
            } else {
                response.render('admin/addAmenity', { data: result });
            }
        });
    } else {
        response.render('admin/addAmenity');
    }
}

const uploadAmenity = (request, response) => {
    pagesModel.uploadAmenity(request, (err, result) => {
        if (err) {
            response.render('admin/addAmenity', { error: err });
        } else {
            response.render('admin/addAmenity', { success: 'Amenity added/updated successfully' });
        }
    });
}

const viewPropertyStatusValues = (request, response) => {
    pagesModel.viewPropertyStatusValues(request, (err, result) => {
        if (err) {
            response.render('admin/viewPropertyStatusValues', { error: err });
        } else {
            response.render('admin/viewPropertyStatusValues', { data: result });
        }
    });
}

const addPropertyStatusValue = (request, response) => {
    const id = parseInt(request.params.id);
    if (id) {
        pagesModel.addPropertyStatusValue(request, (err, result) => {
            if (err) {
                response.render('admin/addPropertyStatusValue', { error: err });
            } else {
                response.render('admin/addPropertyStatusValue', { data: result });
            }
        });
    } else {
        response.render('admin/addPropertyStatusValue');
    }
}

const uploadPropertyStatusValue = (request, response) => {
    pagesModel.uploadPropertyStatusValue(request, (err, result) => {
        if (err) {
            response.render('admin/addPropertyStatusValue', { error: err });
        } else {
            response.render('admin/addPropertyStatusValue', { success: 'Property Status Value added/updated successfully' });
        }
    });
}

const changePropertyStatusValueStatus = (request, response) => {
    pagesModel.changePropertyStatusValueStatus(request, (err, result) => {
        if (err) {
            response.status(400).send('Something went wrong');
        } else {
            if (!result.affectedRows) {
                response.status(200).json({message: 'No Property Status Value Found'});
            } else {
                response.status(200).json({message: 'Status Changed Successfully'});
            }
        }
    });
}

const changePropertyStatusValuePriorityOrder = (request, response) => {
    pagesModel.changePropertyStatusValuePriorityOrder(request, (err, result) => {
        if (err) {
            response.status(400).send('Something went wrong');
        } else {
            if (!result.affectedRows) {
                response.status(200).json({message: 'No Property Status Value Found'});
            } else {
                response.status(200).json({message: 'Priority Order Changed Successfully'});
            }
        }
    });
}

const deletePropertyStatusValue = (request, response) => {
    pagesModel.deletePropertyStatusValue(request, (err, result) => {
        if (err) {
            response.status(400).send('Something went wrong');
        } else {
            if (!result.affectedRows) {
                response.status(200).json({message: 'No Property Status Value Found'});
            } else {
                response.status(200).json({message: 'Property Status Value Deleted Successfully'});
            }
        }
    });
}

const bookingsManagement = (request, response) => {
    pagesModel.bookingsManagement(request, (err, result) => {
        if (err) {
            response.render('admin/bookingsManagement', { error: err });
        } else {
            response.render('admin/bookingsManagement', { data: result });
        }
    });
}

const viewTags = (request, response) => {
    pagesModel.viewTags(request, (err, result) => {
        if (err) {
            response.render('admin/viewTags', { error: err });
        } else {
            response.render('admin/viewTags', { data: result });
        }
    });
}

const addTag = (request, response) => {
    const id = parseInt(request.params.id);
    if (id) {
        pagesModel.addTag(request, (err, result) => {
            if (err) {
                response.render('admin/addTag', { error: err });
            } else {
                response.render('admin/addTag', { data: result });
            }
        });
    } else {
        response.render('admin/addTag');
    }
}

const uploadTag = (request, response) => {
    pagesModel.uploadTag(request, (err, result) => {
        if (err) {
            response.render('admin/addTag', { error: err });
        } else {
            response.render('admin/addTag', { success: 'Tag added/updated successfully' });
        }
    });
}

const changeTagStatus = (request, response) => {
    pagesModel.changeTagStatus(request, (err, result) => {
        if (err) {
            response.status(400).send('Something went wrong');
        } else {
            if (!result.affectedRows) {
                response.status(200).json({message: 'No Tag Found'});
            } else {
                response.status(200).json({message: 'Status Changed Successfully'});
            }
        }
    });
}

const changeTagPriorityOrder = (request, response) => {
    pagesModel.changeTagPriorityOrder(request, (err, result) => {
        if (err) {
            response.status(400).send('Something went wrong');
        } else {
            if (!result.affectedRows) {
                response.status(200).json({message: 'No Tag Found'});
            } else {
                response.status(200).json({message: 'Priority Order Changed Successfully'});
            }
        }
    });
}

const deleteTag = (request, response) => {
    pagesModel.deleteTag(request, (err, result) => {
        if (err) {
            response.status(400).send('Something went wrong');
        } else {
            if (!result.affectedRows) {
                response.status(200).json({message: 'No Tag Found'});
            } else {
                response.status(200).json({message: 'Tag Deleted Successfully'});
            }
        }
    });
}

module.exports = {
    verifyLogin,
    dashboard,
    manageUsers,
    manageSellers,
    viewSellerProperties,
    viewPropertyDetails,
    manageProperties,
    manageServiceSellers,
    viewPropertyTypes,
    addPropertyType,
    uploadPropertyType,
    viewPropertySubTypes,
    addPropertySubType,
    uploadPropertySubType,
    viewServices,
    addService,
    uploadService,
    viewAmenities,
    addAmenity,
    uploadAmenity,
    viewPropertyStatusValues,
    addPropertyStatusValue,
    uploadPropertyStatusValue,
    changePropertyStatusValueStatus,
    changePropertyStatusValuePriorityOrder,
    deletePropertyStatusValue,
    bookingsManagement,
    viewTags,
    addTag,
    uploadTag,
    changeTagStatus,
    changeTagPriorityOrder,
    deleteTag
}