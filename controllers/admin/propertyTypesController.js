const { request, response } = require('express');
const propertyTypesModel = require('../../models/admin/propertyTypesModel');

const propertyTypesList = (request, response) => {
    propertyTypesModel.propertyTypesList(request, (err, result) => {
        if (err) {
            response.status(400).send('Something went wrong');
        } else {
            response.status(200).json({data: result});
        }
    });
}

const addPropertyType = (request, response) => {
    propertyTypesModel.addPropertyType(request, (err, result) => {
        if (err) {
            response.status(400).send('Something went wrong');
        } else {
            response.status(200).json({message: 'Property Type Added Successfully', propertyTypeId: result});
        }
    });
}

const updatePropertyType = (request, response) => {
    propertyTypesModel.updatePropertyType(request, (err, result) => {
        if (err) {
            response.status(400).send('Something went wrong');
        } else {
            if (!result.affectedRows) {
                response.status(200).json({message: 'No Property Type Found'});
            } else {
                response.status(200).json({message: 'Property Type Updated Successfully'});
            }
        }
    });
}

const changePropertyTypeStatus = (request, response) => {
    propertyTypesModel.changePropertyTypeStatus(request, (err, result) => {
        if (err) {
            response.status(400).send('Something went wrong');
        } else {
            if (!result.affectedRows) {
                response.status(200).json({message: 'No Property Type Found'});
            } else {
                response.status(200).json({message: 'Status Changed Successfully'});
            }
        }
    });
}

const changePropertyTypePriorityOrder = (request, response) => {
    propertyTypesModel.changePropertyTypePriorityOrder(request, (err, result) => {
        if (err) {
            response.status(400).send('Something went wrong');
        } else {
            if (!result.affectedRows) {
                response.status(200).json({message: 'No Property Type Found'});
            } else {
                response.status(200).json({message: 'Priority Order Changed Successfully'});
            }
        }
    });
}

const deletePropertyType = (request, response) => {
    propertyTypesModel.deletePropertyType(request, (err, result) => {
        if (err) {
            response.status(400).send('Something went wrong');
        } else {
            if (!result.affectedRows) {
                response.status(200).json({message: 'No Property Type Found'});
            } else {
                response.status(200).json({message: 'Property Type Deleted Successfully'});
            }
        }
    });
}

module.exports = {
    propertyTypesList,
    addPropertyType,
    updatePropertyType,
    changePropertyTypeStatus,
    changePropertyTypePriorityOrder,
    deletePropertyType
}