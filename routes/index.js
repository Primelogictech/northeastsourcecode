var express = require('express');
var router = express.Router();

var isUserLoggedIn = function (request, response, next) {
  const userId = request.session.userId;
  if (userId) {
    next();
  } else {
    response.redirect('/login');
  }
}

const { check, validationResult } = require('express-validator');
const userRegisterLoginController = require('../controllers/user/userRegisterLoginController');
const serviceSellerRegisterController = require('../controllers/user/serviceSellerRegisterController');
const propertyController = require('../controllers/user/propertyController');
const pagesController = require('../controllers/user/pagesController');

var registerValidate = [
  check('name').notEmpty().withMessage('Name is required').isAlpha('en-US', {ignore: ' '}).withMessage('Name should contain alphabets only').trim().escape(),
  // check('email').notEmpty().withMessage('Email is required').isEmail().withMessage('Invalid Email Address').trim().escape().normalizeEmail(),
  check('mobile_number').notEmpty().withMessage('Mobile Number is required').isMobilePhone().withMessage('Invalid Mobile Number').trim().escape(),
  check('password').notEmpty().withMessage('Password is required').isLength({ min: 5 }).withMessage('Password Must Be at Least 5 Characters').trim().escape(),
  // check('confirm_password').notEmpty().withMessage('Confirm Password is required').isLength({ min: 5 }).withMessage('Confirm Password Must Be at Least 5 Characters').trim().escape()
];

var updateUserValidate = [
  check('name').notEmpty().withMessage('Name is required').isAlpha('en-US', {ignore: ' '}).withMessage('Name should contain alphabets only').trim().escape(),
  // check('email').notEmpty().withMessage('Email is required').isEmail().withMessage('Invalid Email Address').trim().escape().normalizeEmail(),
  check('mobile_number').notEmpty().withMessage('Mobile Number is required').isMobilePhone().withMessage('Invalid Mobile Number').trim().escape()
];

var emailValidate = [
  check('email').notEmpty().withMessage('Email is required').isEmail().withMessage('Invalid Email Address').trim().escape().normalizeEmail()
];

var mobileNumberValidate = [
  check('mobile_number').notEmpty().withMessage('Mobile Number is required').isMobilePhone().withMessage('Invalid Mobile Number').trim().escape()
];

var registerAsServiceSellerValidate = [
  // check('name').notEmpty().withMessage('Name is required').isAlpha('en-US', {ignore: ' '}).withMessage('Name should contain alphabets only').trim().escape(),
  // check('email').notEmpty().withMessage('Email is required').isEmail().withMessage('Invalid Email Address').trim().escape().normalizeEmail(),
  // check('mobile_number').notEmpty().withMessage('Mobile Number is required').isMobilePhone().withMessage('Invalid Mobile Number').trim().escape(),
  check('business_experience').notEmpty().withMessage('Business Experience is required').isInt().withMessage('Business Experience should contain numbers only').trim().escape(),
  check('tags').notEmpty().withMessage('Tags field is required').trim().escape(),
  check('about').notEmpty().withMessage('About is required').trim().escape(),
  check('opening_time').notEmpty().withMessage('Opening Time is required').trim().escape(),
  check('closing_time').notEmpty().withMessage('Closing Time is required').trim().escape(),
  // check('services_offered').notEmpty().withMessage('Services Offered is required').trim().escape(),
  // check('images')
  // .custom((value, {req}) => {
  //   if(req.files.images) {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // }).withMessage('Please select image')
  // .custom((value, {req}) => {
  //   if (req.files.images.mimetype === 'image/jpeg'){
  //     return '.jpeg';
  //   } else if (req.files.images.mimetype === 'image/jpg'){
  //     return '.jpg';
  //   } else if (req.files.images.mimetype === 'image/png'){
  //     return '.png';
  //   } else {
  //     return false;
  //   }
  // }).withMessage('Please select images with jpg/jpeg/png format only.')
];

var propertyValidate = [
  check('seller_type').notEmpty().withMessage('Seller Type is required').trim().escape(),
  check('available_for').notEmpty().withMessage('Property available for is required').isInt().withMessage('Invalid value').trim().escape(),
  check('property_title').notEmpty().withMessage('Property Title is required').trim().escape(),
  check('property_type_id').notEmpty().withMessage('Property Type is required').isInt().withMessage('Invalid value').trim().escape(),
  check('property_sub_type_id').notEmpty().withMessage('Property Sub Type is required').isInt().withMessage('Invalid value').trim().escape(),
];

// USER PAGES ROUTES
router.get('/', pagesController.home);

router.get('/home', pagesController.home);

router.get('/login', function(req, res, next) {
  res.render('user/login');
});
router.post('/login', pagesController.login);
router.get('/logout', (req, res) => {
  req.session.destroy();
  res.redirect('/');
});

router.get('/register', function(req, res, next) {
  res.render('user/register');
});
router.post('/register', registerValidate, (req, res, next) => {
  const errors = validationResult(req);
	if (!errors.isEmpty()) {
    return res.render('user/register', { errors: errors.array() });
	} else {
    next();
  }
}, pagesController.register);

router.get('/search_property', pagesController.searchProperty);
router.post('/search_property', pagesController.searchProperty);

router.get('/getMaxPriceInProperties', pagesController.getMaxPriceInProperties);
router.post('/getPropertiesByFilters', pagesController.getPropertiesByFilters);

router.get('/register_as_partner', pagesController.registerServiceSeller);
router.get('/register_as_partner/:id', isUserLoggedIn, pagesController.registerServiceSeller);
router.post('/register_as_partner', registerAsServiceSellerValidate, (req, res, next) => {
  const errors = validationResult(req);
	if (!errors.isEmpty()) {
    return res.render('user/register_as_partner', { errors: errors.array() });
	} else {
    next();
  }
}, pagesController.uploadServiceSeller);

router.get('/post_property', isUserLoggedIn, pagesController.postProperty);
router.get('/post_property/:id', isUserLoggedIn, pagesController.postProperty);
router.post('/post_property', isUserLoggedIn, pagesController.uploadProperty);
router.post('/getPropertySubTypesByPropertyType', pagesController.getPropertySubTypesByPropertyType);

router.get('/property_details/:id', pagesController.getPropertyData);
router.post('/getNearByProperties', pagesController.getNearByProperties);

router.get('/services', pagesController.services);

router.get('/services/:id', pagesController.serviceCategory);

router.get('/service_partner_details/:id/:service_id', pagesController.serviceSellerDetails);

router.get('/getFeaturedProperties', pagesController.getFeaturedProperties);
router.post('/getPropertiesByPropertyType', pagesController.getPropertiesByPropertyType);
router.post('/getShortListedProperties', pagesController.getShortListedProperties);
router.post('/getRecentlyViewedProperties', pagesController.getRecentlyViewedProperties);

router.get('/my_listings', isUserLoggedIn, pagesController.getUserUploadedProperties);
router.get('/edit_profile', isUserLoggedIn, pagesController.editProfile);
router.post('/uploadUserProfilePicture', isUserLoggedIn, pagesController.uploadUserProfilePicture);
router.post('/uploadUserData', isUserLoggedIn, pagesController.uploadUserData);
router.get('/shortListedProperties', isUserLoggedIn, pagesController.shortListedProperties);

router.get('/viewBookings', isUserLoggedIn, pagesController.viewBookings);
router.get('/changeServiceBookingStatus/:id/:status', isUserLoggedIn, pagesController.changeServiceBookingStatus);

router.post('/sendOTP', pagesController.sendOTP);


// USER API ROUTES

router.post('/updateUser', updateUserValidate, (req, res, next) => {
  const errors = validationResult(req);
	if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
	} else {
    next();
  }
}, userRegisterLoginController.updateUser);

router.post('/checkEmail', emailValidate, (req, res, next) => {
  const errors = validationResult(req);
	if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
	} else {
    next();
  }
}, userRegisterLoginController.checkEmail);

router.post('/checkMobileNumber', mobileNumberValidate, (req, res, next) => {
  const errors = validationResult(req);
	if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
	} else {
    next();
  }
}, userRegisterLoginController.checkMobileNumber);

router.post('/registerAsServiceSeller', registerAsServiceSellerValidate, (req, res, next) => {
  const errors = validationResult(req);
	if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
	} else {
    next();
  }
}, serviceSellerRegisterController.registerServiceSeller);

router.post('/isServiceSeller', serviceSellerRegisterController.isServiceSeller);
router.post('/bookService', serviceSellerRegisterController.bookService);
router.post('/isServiceBookedAlready', serviceSellerRegisterController.isServiceBookedAlready);

router.post('/uploadProperty', propertyValidate, (req, res, next) => {
  const errors = validationResult(req);
	if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
	} else {
    next();
  }
}, propertyController.uploadProperty);
router.get('/propertiesList', propertyController.propertiesList);
router.post('/propertiesList', propertyController.propertiesList);

router.post('/addToShortList', propertyController.addToShortList);
router.post('/removeFromShortList', propertyController.removeFromShortList);
router.post('/addToRecentlyViewed', propertyController.addToRecentlyViewed);

router.get('/settings', function(req, res, next) {
  res.render('user/settings');
});

module.exports = router;
