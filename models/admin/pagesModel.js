const { request, response } = require('express');
const db = require('../dbConnection');
var bcrypt = require('bcrypt');
var path = require('path');
const { extname } = require('path');
const fs = require('fs');

const verifyLogin = (request, response) => {
    const { email, password } = request.body;
    db.query("SELECT id, name, email, password FROM users where email = ?", [email], (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            if (result.length > 0) {
                hash = (result[0].password);
                if (bcrypt.compareSync(password, hash)) {
                    response(null, result);
                } else {
                    response('Incorrect Password');
                }
            } else {
                response('Incorrect Email');
            }
        }
    });
}

const countUsers = (request, response) => {
    db.query("SELECT ( SELECT COUNT(*) FROM users ) AS users, ( SELECT COUNT(*) FROM users WHERE is_seller=1 ) AS sellers, ( SELECT COUNT(*) FROM users WHERE is_service_seller=1 ) AS service_sellers, ( SELECT COUNT(*) FROM properties ) AS properties, ( SELECT COUNT(*) FROM services) AS services", (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const manageUsers = (request, response) => {
    const { keyword } = request.body;
    var sql = `SELECT id, name, email, mobile_number, status FROM users WHERE`;
    if (keyword) {
        sql += ` name LIKE N'%${keyword}%' OR email LIKE N'%${keyword}%' OR mobile_number LIKE N'%${keyword}%' AND`;
    }
    sql += ` role = 'User'`;
    db.query(sql,  (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const manageSellers = (request, response) => {
    const { keyword } = request.body;
    var sql = `SELECT id, name, email, mobile_number, alt_mobile_number, status FROM users WHERE`;
    if (keyword) {
        sql += ` ( name LIKE N'%${keyword}%' OR email LIKE N'%${keyword}%' OR mobile_number LIKE N'%${keyword}%' ) AND`;
    }
    sql += ` role = 'User' AND is_seller = 1`;
    db.query(sql,  (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const viewSellerProperties = (request, response) => {
    const id = parseInt(request.params.id);
    db.query("SELECT p.*, GROUP_CONCAT(amenities.name) AS amenities FROM properties AS p LEFT JOIN amenities ON FIND_IN_SET(amenities.id, p.available_amenities) WHERE p.user_id = ? GROUP BY p.id", [id], (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const viewPropertyDetails = (request, response) => {
    const id = parseInt(request.params.id);
    db.query("SELECT p.*, u.name, u.mobile_number, u.about, u.created_at, pt.name as property_type, pst.name as property_sub_type, GROUP_CONCAT(amenities.name) AS amenities FROM properties AS p JOIN users AS u ON p.user_id = u.id LEFT JOIN property_types AS pt ON p.property_type_id = pt.id LEFT JOIN property_sub_types AS pst ON p.property_sub_type_id = pst.id LEFT JOIN amenities ON FIND_IN_SET(amenities.id, p.available_amenities) WHERE p.id = ?", [id], (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const manageProperties = (request, response) => {
    const { keyword, property_type_id, property_sub_type_id } = request.body;
    var sql = `SELECT p.*, pt.name as property_type, pst.name as property_sub_type, GROUP_CONCAT(amenities.name) AS amenities FROM properties AS p LEFT JOIN property_types AS pt ON p.property_type_id = pt.id LEFT JOIN property_sub_types AS pst ON p.property_sub_type_id = pst.id LEFT JOIN amenities ON FIND_IN_SET(amenities.id, p.available_amenities)`;
    if (keyword || property_type_id || property_sub_type_id) {
        sql += ` WHERE`;
    }
    if (keyword) {
        sql += ` p.property_title LIKE N'%${keyword}%' OR p.society_name LIKE N'%${keyword}%' OR p.city LIKE N'%${keyword}%'`;
    }
    if (property_type_id) {
        sql += ` AND`;
    }
    if (property_type_id) {
        sql += ` p.property_type_id = '${property_type_id}'`;
    }
    if (property_sub_type_id) {
        sql += ` AND`;
    }
    if (property_sub_type_id) {
        sql += ` p.property_sub_type_id = '${property_sub_type_id}'`;
    }
    sql += ` GROUP BY p.id ORDER BY p.id DESC`;
    db.query(sql,  (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const manageServiceSellers = (request, response) => {
    const { keyword } = request.body;
    var sql = `SELECT u.id, u.name, u.email, u.mobile_number, u.status, s.business_experience, s.tags, s.about, s.images, s.opening_time, s.closing_time, GROUP_CONCAT(services.name) AS services FROM users AS u LEFT JOIN service_sellers AS s ON u.id=s.user_id LEFT JOIN services ON FIND_IN_SET(services.id, s.services_offered) WHERE`;
    if (keyword) {
        sql += ` u.name LIKE N'%${keyword}%' OR u.email LIKE N'%${keyword}%' OR u.mobile_number LIKE N'%${keyword}%' AND`;
    }
    sql += ` u.is_service_seller = 1 GROUP BY u.id`;
    db.query(sql,  (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const viewPropertyTypes = (request, response) => {
    const { keyword } = request.body;
    var sql = `SELECT * FROM property_types`;
    if (keyword) {
        sql += ` WHERE name LIKE N'%${keyword}%'`;
    }
    sql += ` ORDER BY priority_order`;
    db.query(sql, (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const addPropertyType = (request, response) => {
    const id = parseInt(request.params.id);
    db.query('SELECT * FROM property_types WHERE id =?', [id], (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const uploadPropertyType = (request, response) => {
    const { id, name, priority_order } = request.body;
    if (id) {
        db.query('UPDATE property_types SET name = ?, priority_order = ? WHERE id = ?', [name, priority_order, id], (error, result) => {
            if (error) {
                response('Database connection error');
            } else {
                response(null, result);
            }
        });
    } else {
        db.query("INSERT INTO property_types (name, priority_order) VALUES (?, ?)", [name, priority_order], (error, result) => {
            if (error) {
                response('Database connection error');
            } else {
                response(null, result.insertId);
            }
        });
    }
}

const viewPropertySubTypes = (request, response) => {
    const { keyword, property_type_id } = request.body;
    var sql = `SELECT pst.id, pst.name AS property_sub_type_name, pst.status, pst.priority_order, pt.name AS property_type_name FROM property_sub_types AS pst LEFT JOIN property_types AS pt ON pst.property_type_id=pt.id`;
    if (property_type_id || keyword){
        sql += ` WHERE`;
    }
    if (property_type_id) {
        sql += ` pst.property_type_id = '${property_type_id}'`;
    }
    if (property_type_id && keyword) {
        sql += ` AND`;
    }
    if (keyword) {
        sql += ` pst.name LIKE N'%${keyword}%'`;
    }
    sql += ` ORDER BY pst.priority_order`;
    db.query(sql,  (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const addPropertySubType = (request, response) => {
    const id = parseInt(request.params.id);
    db.query('SELECT * FROM property_sub_types WHERE id =?', [id], (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const uploadPropertySubType = (request, response) => {
    const { id, property_type_id, name, priority_order } = request.body;
    if (id) {
        db.query('UPDATE property_sub_types SET property_type_id = ?, name = ?, priority_order = ? WHERE id = ?', [property_type_id, name, priority_order, id], (error, result) => {
            if (error) {
                response('Database connection error');
            } else {
                response(null, result);
            }
        });
    } else {
        db.query("INSERT INTO property_sub_types (property_type_id, name, priority_order) VALUES (?, ?, ?)", [property_type_id, name, priority_order], (error, result) => {
            if (error) {
                response('Database connection error');
            } else {
                response(null, result.insertId);
            }
        });
    }
}

const viewServices = (request, response) => {
    const { keyword } = request.body;
    var sql = `SELECT * FROM services `;
    if (keyword) {
        sql += ` WHERE name LIKE N'%${keyword}%'`;
    }
    sql += ` ORDER BY priority_order`;
    db.query(sql, (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const addService = (request, response) => {
    const id = parseInt(request.params.id);
    db.query('SELECT * FROM services WHERE id =?', [id], (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const uploadService = (request, response) => {
    const { id, name, priority_order } = request.body;
    if (id) {
        if (request.files && request.files.image) {
            db.query('SELECT image FROM services where id = ?', [id], (error, result) => {
                if (error) {
                    response(error);
                } else {
                    if (result) {
                        if (result[0].image) {
                            var image_path = path.join(__dirname, '../../uploads/') + result[0].image;
                            fs.unlink(image_path, (err) => {
                                if (err) {
                                    response(err);
                                }
                            });
                        }
                    }
                }
            });
            sampleFile = request.files.image;
            var timestamp = Date.now();
            var randomString = ("" + Math.random()).substring(2, 8);
            var fileName = timestamp + randomString + path.extname(sampleFile.name);
            uploadPath = path.join(__dirname, '../../uploads/') + fileName;
            sampleFile.mv(uploadPath);
            const image = fileName;

            db.query('UPDATE services SET name = ?, image = ?, priority_order = ? WHERE id = ?', [name, image, priority_order, id], (error, result) => {
                if (error) {
                    response('Database connection error');
                } else {
                    response(null, result);
                }
            });
        } else {
            db.query('UPDATE services SET name = ?, priority_order = ? WHERE id = ?', [name, priority_order, id], (error, result) => {
                if (error) {
                    response('Database connection error');
                } else {
                    response(null, result);
                }
            });
        }
    } else {
        if (request.files.image) {
            sampleFile = request.files.image;
            var timestamp = Date.now();
            var randomString = ("" + Math.random()).substring(2, 8);
            var fileName = timestamp + randomString + path.extname(sampleFile.name);
            uploadPath = path.join(__dirname, '../../uploads/') + fileName;
            sampleFile.mv(uploadPath);
            const image = fileName;

            db.query("INSERT INTO services (name, image, priority_order) VALUES (?, ?, ?)", [name, image, priority_order], (error, result) => {
                if (error) {
                    response('Database connection error');
                } else {
                    response(null, result.insertId);
                }
            });
        } else {
            return response('Service Image is required');
        }
    }
}

const viewAmenities = (request, response) => {
    const { keyword } = request.body;
    var sql = `SELECT * FROM amenities`;
    if (keyword) {
        sql += ` WHERE name LIKE N'%${keyword}%'`;
    }
    sql += ` ORDER BY priority_order`;
    db.query(sql, (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const addAmenity = (request, response) => {
    const id = parseInt(request.params.id);
    db.query('SELECT * FROM amenities WHERE id =?', [id], (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const uploadAmenity = (request, response) => {
    const { id, name, priority_order } = request.body;
    if (id) {
        db.query('UPDATE amenities SET name = ?, priority_order = ? WHERE id = ?', [name, priority_order, id], (error, result) => {
            if (error) {
                response('Database connection error');
            } else {
                response(null, result);
            }
        });
    } else {
        db.query("INSERT INTO amenities (name, priority_order) VALUES (?, ?)", [name, priority_order], (error, result) => {
            if (error) {
                response('Database connection error');
            } else {
                response(null, result.insertId);
            }
        });
    }
}

const viewPropertyStatusValues = (request, response) => {
    const { keyword } = request.body;
    var sql = `SELECT * FROM property_status_values`;
    if (keyword) {
        sql += ` WHERE name LIKE N'%${keyword}%'`;
    }
    sql += ` ORDER BY priority_order`;
    db.query(sql, (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const addPropertyStatusValue = (request, response) => {
    const id = parseInt(request.params.id);
    db.query('SELECT * FROM property_status_values WHERE id =?', [id], (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const uploadPropertyStatusValue = (request, response) => {
    const { id, name, priority_order } = request.body;
    if (id) {
        db.query('UPDATE property_status_values SET name = ?, priority_order = ? WHERE id = ?', [name, priority_order, id], (error, result) => {
            if (error) {
                response('Database connection error');
            } else {
                response(null, result);
            }
        });
    } else {
        db.query("INSERT INTO property_status_values (name, priority_order) VALUES (?, ?)", [name, priority_order], (error, result) => {
            if (error) {
                response('Database connection error');
            } else {
                response(null, result.insertId);
            }
        });
    }
}

const changePropertyStatusValueStatus = (request, response) => {
    const id = parseInt(request.params.id);
    const status = parseInt(request.params.status);
    db.query('UPDATE property_status_values SET status = ? WHERE id = ?', [status, id], (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const changePropertyStatusValuePriorityOrder = (request, response) => {
    const id = parseInt(request.params.id);
    const priority_order = parseInt(request.params.priority_order);
    db.query('UPDATE property_status_values SET priority_order = ? WHERE id = ?', [priority_order, id], (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const deletePropertyStatusValue = (request, response) => {
    const id = parseInt(request.params.id);
    db.query('DELETE FROM property_status_values WHERE id = ?', [id], (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const bookingsManagement = (request, response) => {
    const { keyword } = request.body;
    var sql = `SELECT sb.*, u.name, u.mobile_number, s.name AS service_name, ss.business_name FROM service_bookings AS sb LEFT JOIN users AS u ON sb.user_id = u.id LEFT JOIN services AS s ON sb.service_id = s.id LEFT JOIN service_sellers AS ss ON sb.service_seller_id = ss.user_id`;
    if (keyword) {
        sql += ` WHERE u.name LIKE N'%${keyword}%' OR ss.business_name LIKE N'%${keyword}%' OR u.mobile_number LIKE N'%${keyword}%'`;
    }
    db.query(sql,  (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const viewTags = (request, response) => {
    const { keyword } = request.body;
    var sql = `SELECT * FROM tags`;
    if (keyword) {
        sql += ` WHERE name LIKE N'%${keyword}%'`;
    }
    sql += ` ORDER BY priority_order`;
    db.query(sql, (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const addTag = (request, response) => {
    const id = parseInt(request.params.id);
    db.query('SELECT * FROM tags WHERE id =?', [id], (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const uploadTag = (request, response) => {
    const { id, name, priority_order } = request.body;
    if (id) {
        db.query('UPDATE tags SET name = ?, priority_order = ? WHERE id = ?', [name, priority_order, id], (error, result) => {
            if (error) {
                response('Database connection error');
            } else {
                response(null, result);
            }
        });
    } else {
        db.query("INSERT INTO tags (name, priority_order) VALUES (?, ?)", [name, priority_order], (error, result) => {
            if (error) {
                response('Database connection error');
            } else {
                response(null, result.insertId);
            }
        });
    }
}

const changeTagStatus = (request, response) => {
    const id = parseInt(request.params.id);
    const status = parseInt(request.params.status);
    db.query('UPDATE tags SET status = ? WHERE id = ?', [status, id], (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const changeTagPriorityOrder = (request, response) => {
    const id = parseInt(request.params.id);
    const priority_order = parseInt(request.params.priority_order);
    db.query('UPDATE tags SET priority_order = ? WHERE id = ?', [priority_order, id], (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const deleteTag = (request, response) => {
    const id = parseInt(request.params.id);
    db.query('DELETE FROM property_status_values WHERE id = ?', [id], (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

module.exports = {
    verifyLogin,
    countUsers,
    manageUsers,
    manageSellers,
    viewSellerProperties,
    viewPropertyDetails,
    manageProperties,
    manageServiceSellers,
    viewPropertyTypes,
    addPropertyType,
    uploadPropertyType,
    viewPropertySubTypes,
    addPropertySubType,
    uploadPropertySubType,
    viewServices,
    addService,
    uploadService,
    viewAmenities,
    addAmenity,
    uploadAmenity,
    viewPropertyStatusValues,
    addPropertyStatusValue,
    uploadPropertyStatusValue,
    changePropertyStatusValueStatus,
    changePropertyStatusValuePriorityOrder,
    deletePropertyStatusValue,
    bookingsManagement,
    viewTags,
    addTag,
    uploadTag,
    changeTagStatus,
    changeTagPriorityOrder,
    deleteTag
}