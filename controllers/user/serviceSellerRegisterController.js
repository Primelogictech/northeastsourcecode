const { request, response } = require('express');
const userRegisterLoginModel = require('../../models/user/userRegisterLoginModel');
const serviceSellerRegisterModel = require('../../models/user/serviceSellerRegisterModel');

const registerServiceSeller = (request, response) => {
    const { user_id } = request.body;
    if (user_id) {
        serviceSellerRegisterModel.registerServiceSeller(request, (err, result) => {
            if (err) {
                response.status(400).send(err);
            } else {
                response.status(200).json({message: 'Partner Registration Successful', userId: result});
            }
        });
    } else {
        userRegisterLoginModel.checkEmail(request, (err, result) => {
            if (err) {
                response.status(400).send(err);
            } else {
                if (result.length > 0) {
                    response.status(200).json({message: 'Email Taken'});
                } else {
                    userRegisterLoginModel.checkMobileNumber(request, (err, result) => {
                        if (err) {
                            response.status(400).send(err);
                        } else {
                            if (result.length > 0) {
                                response.status(200).json({message: 'Mobile Number Taken'});
                            } else {
                                serviceSellerRegisterModel.registerServiceSeller(request, (err, result) => {
                                    if (err) {
                                        response.status(400).send(err);
                                    } else {
                                        response.status(200).json({message: 'Partner Registration Successful', userId: result});
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });
    }
}

const isServiceSeller = (request, response) => {
    serviceSellerRegisterModel.isServiceSeller(request, (err, result) => {
        if (err) {
            response.status(400).send(err);
        } else {
            if (result.length > 0 && result[0].is_service_seller) {
                response.status(200).json({message: 'Yes'});
            } else {
                response.status(200).json({message: 'No'});
            }
        }
    });
}

const bookService = (request, response) => {
    serviceSellerRegisterModel.bookService(request, (err, result) => {
        if (err) {
            response.status(400).send(err);
        } else {
            response.status(200).json({message: 'Service Booked', booking_id: result});
        }
    });
}

const isServiceBookedAlready = (request, response) => {
    serviceSellerRegisterModel.isServiceBookedAlready(request, (err, result) => {
        if (err) {
            response.status(400).send(err);
        } else {
            if (result.length > 0 && result[0].id) {
                response.status(200).json({message: 'Yes'});
            } else {
                response.status(200).json({message: 'No'});
            }
        }
    });
}

module.exports = {
    registerServiceSeller,
    isServiceSeller,
    bookService,
    isServiceBookedAlready
}