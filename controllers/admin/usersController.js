const { request, response } = require('express');
const usersModel = require('../../models/admin/usersModel');

const usersList = (request, response) => {
    usersModel.usersList(request, (err, result) => {
        if (err) {
            response.status(400).send('Something went wrong');
        } else {
            response.status(200).json({data: result});
        }
    });
}

const getUserData = (request, response) => {
    usersModel.getUserData(request, (err, result) => {
        if (err) {
            response.status(400).send('Something went wrong');
        } else {
            response.status(200).json({users: result});
        }
    });
}

const changeUserStatus = (request, response) => {
    usersModel.changeUserStatus(request, (err, result) => {
        if (err) {
            response.status(400).send('Something went wrong');
        } else {
            if (!result.affectedRows) {
                response.status(200).json({message: 'No User Found'});
            } else {
                response.status(200).json({message: 'Status Changed Successfully'});
            }
        }
    });
}

const deleteUser = (request, response) => {
    usersModel.deleteUser(request, (err, result) => {
        if (err) {
            response.status(400).send('Something went wrong');
        } else {
            if (!result.affectedRows) {
                response.status(200).json({message: 'No User Found'});
            } else {
                response.status(200).json({message: 'User Deleted Successfully'});
            }
        }
    });
}

module.exports = {
    usersList,
    getUserData,
    changeUserStatus,
    deleteUser
}