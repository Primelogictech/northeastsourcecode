const { request, response } = require('express');
const db = require('../dbConnection');
var bcrypt = require('bcrypt');
var path = require('path');
const { extname } = require('path');

const registerServiceSeller = (request, response) => {
    const { user_id, name, email, mobile_number, business_experience, tags, about, opening_time, closing_time, services_offered } = request.body;
    if (user_id) {
        db.query("SELECT id FROM users WHERE id = ?", [user_id], (error, result) => {
            if (error) {
                response('Database connection error');
            } else {
                if (result.length > 0) {
                    if (business_experience && tags && about && opening_time && closing_time && services_offered) {
                        let sampleFile;
                        let uploadPath;
                        let imagesArray = [];
                        if (request.files.images.length > 1) {
                            request.files.images.forEach(element => {
                                var timestamp = Date.now();
                                var randomString = ("" + Math.random()).substring(2, 8);
                                var fileName = timestamp + randomString + path.extname(element.name);
                                uploadPath = path.join(__dirname, '../../uploads/') + fileName;
                                element.mv(uploadPath);
                                imagesArray.push(fileName);
                            });
                        } else {
                            sampleFile = request.files.images;
                            var timestamp = Date.now();
                            var randomString = ("" + Math.random()).substring(2, 8);
                            var fileName = timestamp + randomString + path.extname(sampleFile.name);
                            uploadPath = path.join(__dirname, '../../uploads/') + fileName;
                            sampleFile.mv(uploadPath);
                            imagesArray.push(fileName);
                        }
                        const images = imagesArray.join(', ');
                        db.query("INSERT INTO service_sellers (user_id, business_experience, tags, about, images, opening_time, closing_time, services_offered) VALUES (?, ?, ?, ?, ?, ?, ?, ?)", [user_id, business_experience, tags, about, images, opening_time, closing_time, services_offered], (error, result) => {
                            if (error) {
                                response('Database connection error');
                            } else {
                                if (result.insertId) {
                                    db.query('UPDATE users SET is_service_seller = 1 WHERE id = ?', [user_id], (error, result) => {
                                        if (error) {
                                            response('Database connection error');
                                        } else {
                                            response(null, user_id);
                                        }
                                    });
                                } else {
                                    response('Something went wrong');
                                }
                            }
                        });
                    } else {
                        response('Please input all fields');
                    }
                } else {
                    response('No User Found');
                }
            }
        });
    } else {
        if (name && email && mobile_number && business_experience && tags && about && opening_time && closing_time && services_offered) {
            password = "service@123";
            const hashedPassword = bcrypt.hashSync(password, 10);
            db.query("INSERT INTO users (name, email, mobile_number, password, role, is_service_seller) VALUES (?, ?, ?, ?, ?, ?)", [name, email, mobile_number, hashedPassword, 'Service Seller', '1'], (error, result) => {
                if (error) {
                    response('Database connection error');
                } else {
                    if (result.insertId) {
                        const userId = result.insertId;
                        let sampleFile;
                        let uploadPath;
                        let imagesArray = [];
                        if (request.files.images.length > 1) {
                            request.files.images.forEach(element => {
                                var timestamp = Date.now();
                                var randomString = ("" + Math.random()).substring(2, 8);
                                var fileName = timestamp + randomString + path.extname(element.name);
                                uploadPath = path.join(__dirname, '../../uploads/') + fileName;
                                element.mv(uploadPath);
                                imagesArray.push(uploadPath);
                            });
                        } else {
                            sampleFile = request.files.images;
                            var timestamp = Date.now();
                            var randomString = ("" + Math.random()).substring(2, 8);
                            var fileName = timestamp + randomString + path.extname(sampleFile.name);
                            uploadPath = path.join(__dirname, '../../uploads/') + fileName;
                            sampleFile.mv(uploadPath);
                            imagesArray.push(uploadPath);
                        }
                        const images = imagesArray.join(', ');
                        db.query("INSERT INTO service_sellers (user_id, business_experience, tags, about, images, opening_time, closing_time, services_offered) VALUES (?, ?, ?, ?, ?, ?, ?, ?)", [userId, business_experience, tags, about, images, opening_time, closing_time, services_offered], (error, result) => {
                            if (error) {
                                response('Database connection error');
                            } else {
                                response(null, userId);
                            }
                        });
                    } else {
                        response('Something went wrong while creating service seller');
                    }
                }
            });
        } else {
            response('Please input all fields');
        }
    }
}

const isServiceSeller = (request, response) => {
    const user_id = parseInt(request.body.user_id);
    db.query("SELECT is_service_seller FROM users WHERE id = ?", [user_id], (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const bookService = (request, response) => {
    const { user_id, service_id, service_seller_id } = request.body;
    db.query("INSERT INTO service_bookings (user_id, service_id, service_seller_id) VALUES (?, ?, ?)", [user_id, service_id, service_seller_id], (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result.insertId);
        }
    });
}

const isServiceBookedAlready = (request, response) => {
    const { user_id, service_id, service_seller_id } = request.body;
    db.query("SELECT id FROM service_bookings WHERE user_id = ? AND service_id = ? AND service_seller_id = ?", [user_id, service_id, service_seller_id], (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

module.exports = {
    registerServiceSeller,
    isServiceSeller,
    bookService,
    isServiceBookedAlready
}