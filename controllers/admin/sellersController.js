const { request, response } = require('express');
const sellersModel = require('../../models/admin/sellersModel');

const sellersList = (request, response) => {
    sellersModel.sellersList(request, (err, result) => {
        if (err) {
            response.status(400).send(err);
        } else {
            response.status(200).json({data: result});
        }
    });
}

const getSellerDetails = (request, response) => {
    sellersModel.getSellerDetails(request, (err, result) => {
        if (err) {
            response.status(400).send(err);
        } else {
            response.status(200).json({data: result});
        }
    });
}

const getSellerProperties = (request, response) => {
    sellersModel.getSellerProperties(request, (err, result) => {
        if (err) {
            response.status(400).send(err);
        } else {
            response.status(200).json({data: result});
        }
    });
}

const changeSellerStatus = (request, response) => {
    sellersModel.changeSellerStatus(request, (err, result) => {
        if (err) {
            response.status(400).send(err);
        } else {
            if (!result.affectedRows) {
                response.status(200).json({message: 'No Seller Found'});
            } else {
                response.status(200).json({message: 'Status Changed Successfully'});
            }
        }
    });
}

const deleteSeller = (request, response) => {
    sellersModel.deleteSeller(request, (err, result) => {
        if (err) {
            response.status(400).send(err);
        } else {
            if (!result.affectedRows) {
                response.status(200).json({message: 'No Seller Found'});
            } else {
                response.status(200).json({message: 'User Deleted Successfully'});
            }
        }
    });
}

module.exports = {
    sellersList,
    getSellerDetails,
    getSellerProperties,
    changeSellerStatus,
    deleteSeller
}