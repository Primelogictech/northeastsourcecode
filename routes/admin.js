var express = require('express');
var router = express.Router();

const verifyToken = require('./verifyAdminToken');

const { check, validationResult } = require('express-validator');
const usersController = require('../controllers/admin/usersController');
const amenitiesController = require('../controllers/admin/amenitiesController');
const propertyTypesController = require('../controllers/admin/propertyTypesController');
const propertySubTypesController = require('../controllers/admin/propertySubTypesController');
const servicesController = require('../controllers/admin/servicesController');
const serviceSellersController = require('../controllers/admin/serviceSellersController');
const propertiesController = require('../controllers/admin/propertiesController');
const sellersController = require('../controllers/admin/sellersController');
const pagesController = require('../controllers/admin/pagesController');

var amenityValidate = [
  check('name', 'Amenity Name is required').notEmpty().trim().escape(),
  check('priority_order').trim().escape()
];

var propertyTypeValidate = [
  check('name', 'Property Type Name is required').notEmpty().trim().escape(),
  check('priority_order').trim().escape()
];

var propertySubTypeValidate = [
  check('property_type_id').notEmpty().withMessage('Property Type is required').isInt().withMessage('Invalid Value').trim().escape(),
  check('name', 'Property Sub Type Name is required').notEmpty().trim().escape(),
  check('priority_order').trim().escape()
];

var serviceValidate = [
  check('name', 'Service Name is required').notEmpty().trim().escape(),
  check('priority_order').trim().escape()
];

var propertyStatusValueValidate = [
  check('name', 'Property Status Value is required').notEmpty().trim().escape(),
  check('priority_order').trim().escape()
]

var tagValidate = [
  check('name', 'Tag Name is required').notEmpty().trim().escape(),
  check('priority_order').trim().escape()
]

// ADMIN PAGES ROUTES
router.get('/', function(req, res, next) {
  res.render('admin/login');
});
router.get('/login', function(req, res, next) {
  res.render('admin/login');
});
router.post('/login', pagesController.verifyLogin);
router.get('/index', verifyToken, pagesController.dashboard);
router.get('/logout', verifyToken, (req, res) => {
  res.clearCookie('token');
  res.redirect('/admin');
});

router.get('/manageUsers', verifyToken, pagesController.manageUsers);
router.get('/manageSellers', verifyToken, pagesController.manageSellers);
router.get('/viewSellerProperties/:id', verifyToken, pagesController.viewSellerProperties);

router.get('/manageProperties', verifyToken, pagesController.manageProperties);
router.post('/manageProperties', verifyToken, pagesController.manageProperties);

router.get('/viewPropertyDetails/:id', verifyToken, pagesController.viewPropertyDetails);

router.get('/manageServiceSellers', verifyToken, pagesController.manageServiceSellers);
router.post('/manageServiceSellers', verifyToken, pagesController.manageServiceSellers);

// Property Types
router.get('/viewPropertyTypes', verifyToken, pagesController.viewPropertyTypes);
router.get('/addPropertyType', verifyToken, pagesController.addPropertyType);
router.get('/addPropertyType/:id', verifyToken, pagesController.addPropertyType);
router.post('/addPropertyType', verifyToken, propertyTypeValidate, (req, res, next) => {
  const errors = validationResult(req);
	if (!errors.isEmpty()) {
    return res.render('admin/addPropertyType', { errors: errors.array() });
	} else {
    next();
  }
}, pagesController.uploadPropertyType);
router.post('/addPropertyType/:id', verifyToken, propertyTypeValidate, (req, res, next) => {
  const errors = validationResult(req);
	if (!errors.isEmpty()) {
    return res.render('admin/addPropertyType', { errors: errors.array() });
	} else {
    next();
  }
}, pagesController.uploadPropertyType);

// Property Sub Types
router.get('/viewPropertySubTypes', verifyToken, pagesController.viewPropertySubTypes);
router.get('/addPropertySubType', verifyToken, pagesController.addPropertySubType);
router.get('/addPropertySubType/:id', verifyToken, pagesController.addPropertySubType);
router.post('/addPropertySubType', verifyToken, propertySubTypeValidate, (req, res, next) => {
  const errors = validationResult(req);
	if (!errors.isEmpty()) {
    return res.render('admin/addPropertySubType', { errors: errors.array() });
	} else {
    next();
  }
}, pagesController.uploadPropertySubType);
router.post('/addPropertySubType/:id', verifyToken, propertySubTypeValidate, (req, res, next) => {
  const errors = validationResult(req);
	if (!errors.isEmpty()) {
    return res.render('admin/addPropertySubType', { errors: errors.array() });
	} else {
    next();
  }
}, pagesController.uploadPropertySubType);

// Services
router.get('/viewServices', verifyToken, pagesController.viewServices);
router.get('/addService', verifyToken, pagesController.addService);
router.get('/addService/:id', verifyToken, pagesController.addService);
router.post('/addService', verifyToken, serviceValidate, (req, res, next) => {
  const errors = validationResult(req);
	if (!errors.isEmpty()) {
    return res.render('admin/addService', { errors: errors.array() });
	} else {
    next();
  }
}, pagesController.uploadService);
router.post('/addService/:id', verifyToken, serviceValidate, (req, res, next) => {
  const errors = validationResult(req);
	if (!errors.isEmpty()) {
    return res.render('admin/addService', { errors: errors.array() });
	} else {
    next();
  }
}, pagesController.uploadService);

// Amenities
router.get('/viewAmenities', verifyToken, pagesController.viewAmenities);
router.get('/addAmenity', verifyToken, pagesController.addAmenity);
router.get('/addAmenity/:id', verifyToken, pagesController.addAmenity);
router.post('/addAmenity', verifyToken, amenityValidate, (req, res, next) => {
  const errors = validationResult(req);
	if (!errors.isEmpty()) {
    return res.render('admin/addAmenity', { errors: errors.array() });
	} else {
    next();
  }
}, pagesController.uploadAmenity);
router.post('/addAmenity/:id', verifyToken, amenityValidate, (req, res, next) => {
  const errors = validationResult(req);
	if (!errors.isEmpty()) {
    return res.render('admin/addAmenity', { errors: errors.array() });
	} else {
    next();
  }
}, pagesController.uploadAmenity);

// Property Status Values
router.get('/viewPropertyStatusValues', verifyToken, pagesController.viewPropertyStatusValues);
router.get('/addPropertyStatusValue', verifyToken, pagesController.addPropertyStatusValue);
router.get('/addPropertyStatusValue/:id', verifyToken, pagesController.addPropertyStatusValue);
router.post('/addPropertyStatusValue', verifyToken, propertyStatusValueValidate, (req, res, next) => {
  const errors = validationResult(req);
	if (!errors.isEmpty()) {
    return res.render('admin/addPropertyStatusValue', { errors: errors.array() });
	} else {
    next();
  }
}, pagesController.uploadPropertyStatusValue);
router.post('/addPropertyStatusValue/:id', verifyToken, propertyStatusValueValidate, (req, res, next) => {
  const errors = validationResult(req);
	if (!errors.isEmpty()) {
    return res.render('admin/addPropertyStatusValue', { errors: errors.array() });
	} else {
    next();
  }
}, pagesController.uploadPropertyStatusValue);
router.get('/changePropertyStatusValueStatus/:id/:status', verifyToken, pagesController.changePropertyStatusValueStatus);
router.get('/changePropertyStatusValuePriorityOrder/:id/:priority_order', verifyToken, pagesController.changePropertyStatusValuePriorityOrder);
router.get('/deletePropertyStatusValue/:id', verifyToken, pagesController.deletePropertyStatusValue);

router.get('/bookingsManagement', verifyToken, pagesController.bookingsManagement);

// Tags
router.get('/viewTags', verifyToken, pagesController.viewTags);
router.get('/addTag', verifyToken, pagesController.addTag);
router.get('/addTag/:id', verifyToken, pagesController.addTag);
router.post('/addTag', verifyToken, tagValidate, (req, res, next) => {
  const errors = validationResult(req);
	if (!errors.isEmpty()) {
    return res.render('admin/addTag', { errors: errors.array() });
	} else {
    next();
  }
}, pagesController.uploadTag);
router.post('/addTag/:id', verifyToken, tagValidate, (req, res, next) => {
  const errors = validationResult(req);
	if (!errors.isEmpty()) {
    return res.render('admin/addTag', { errors: errors.array() });
	} else {
    next();
  }
}, pagesController.uploadTag);
router.get('/changeTagStatus/:id/:status', verifyToken, pagesController.changeTagStatus);
router.get('/changeTagPriorityOrder/:id/:priority_order', verifyToken, pagesController.changeTagPriorityOrder);
router.get('/deleteTag/:id', verifyToken, pagesController.deleteTag);




// API ROUTES
// User Routes
router.route('/users').get(usersController.usersList);
router.route('/users').post(usersController.usersList);
router.route('/users/getUserData/:id').get(usersController.getUserData);
router.route('/users/changeUserStatus/:id/:status').get(usersController.changeUserStatus);
router.route('/users/deleteUser/:id').get(usersController.deleteUser);

// Amenities Routes
router.route('/amenities').get(amenitiesController.amenitiesList);
router.route('/amenities').post(amenitiesController.amenitiesList);
router.post('/amenities/addAmenity', amenityValidate, (req, res, next) => {
  const errors = validationResult(req);
	if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
	} else {
    next();
  }
}, amenitiesController.addAmenity);
router.post('/amenities/updateAmenity', amenityValidate, (req, res, next) => {
  const errors = validationResult(req);
	if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
	} else {
    next();
  }
}, amenitiesController.updateAmenity);
router.route('/amenities/changeAmenityStatus/:id/:status').get(amenitiesController.changeAmenityStatus);
router.route('/amenities/changeAmenityPriorityOrder/:id/:priority_order').get(amenitiesController.changeAmenityPriorityOrder);
router.route('/amenities/deleteAmenity/:id').get(amenitiesController.deleteAmenity);

// Property Types Routes
router.route('/propertyTypes').get(propertyTypesController.propertyTypesList);
router.route('/propertyTypes').post(propertyTypesController.propertyTypesList);
router.post('/propertyTypes/addPropertyType', propertyTypeValidate, (req, res, next) => {
  const errors = validationResult(req);
	if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
	} else {
    next();
  }
}, propertyTypesController.addPropertyType);
router.post('/propertyTypes/updatePropertyType', propertyTypeValidate, (req, res, next) => {
  const errors = validationResult(req);
	if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
	} else {
    next();
  }
}, propertyTypesController.updatePropertyType);
router.route('/propertyTypes/changePropertyTypeStatus/:id/:status').get(propertyTypesController.changePropertyTypeStatus);
router.route('/propertyTypes/changePropertyTypePriorityOrder/:id/:priority_order').get(propertyTypesController.changePropertyTypePriorityOrder);
router.route('/propertyTypes/deletePropertyType/:id').get(propertyTypesController.deletePropertyType);

// Property Sub Types Routes
router.route('/propertySubTypes').get(propertySubTypesController.propertySubTypesList);
router.route('/propertySubTypes').post(propertySubTypesController.propertySubTypesList);
router.post('/propertySubTypes/addPropertySubType', propertySubTypeValidate, (req, res, next) => {
  const errors = validationResult(req);
	if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
	} else {
    next();
  }
}, propertySubTypesController.addPropertySubType);
router.post('/propertySubTypes/updatePropertySubType', propertySubTypeValidate, (req, res, next) => {
  const errors = validationResult(req);
	if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
	} else {
    next();
  }
}, propertySubTypesController.updatePropertySubType);
router.route('/propertySubTypes/changePropertySubTypeStatus/:id/:status').get(propertySubTypesController.changePropertySubTypeStatus);
router.route('/propertySubTypes/changePropertySubTypePriorityOrder/:id/:priority_order').get(propertySubTypesController.changePropertySubTypePriorityOrder);
router.route('/propertySubTypes/deletePropertySubType/:id').get(propertySubTypesController.deletePropertySubType);

// Services Routes
router.route('/services').get(servicesController.servicesList);
router.route('/services').post(servicesController.servicesList);
router.post('/services/addService', serviceValidate, (req, res, next) => {
  const errors = validationResult(req);
	if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
	} else {
    next();
  }
}, servicesController.addService);
router.post('/services/updateService', serviceValidate, (req, res, next) => {
  const errors = validationResult(req);
	if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
	} else {
    next();
  }
}, servicesController.updateService);
router.route('/services/changeServiceStatus/:id/:status').get(servicesController.changeServiceStatus);
router.route('/services/changeServicePriorityOrder/:id/:priority_order').get(servicesController.changeServicePriorityOrder);
router.route('/services/deleteService/:id').get(servicesController.deleteService);

// Service Sellers Routes
router.route('/serviceSellers').get(serviceSellersController.serviceSellersList);
router.route('/serviceSellers').post(serviceSellersController.serviceSellersList);
router.route('/serviceSellers/getServiceSellerDetails/:id').get(serviceSellersController.getServiceSellerDetails);
router.route('/serviceSellers/changeServiceSellerStatus/:id/:status').get(serviceSellersController.changeServiceSellerStatus);
router.route('/serviceSellers/deleteServiceSeller/:id').get(serviceSellersController.deleteServiceSeller);

// Properties Routes
router.route('/properties').get(propertiesController.propertiesList);
router.route('/properties').post(propertiesController.propertiesList);
router.route('/properties/getPropertyDetails/:id').get(propertiesController.getPropertyDetails);
router.route('/properties/changePropertyStatus/:id/:status').get(propertiesController.changePropertyStatus);
router.route('/properties/changeFeaturedStatus/:id/:status').get(propertiesController.changeFeaturedStatus);
router.route('/properties/deleteProperty/:id').get(propertiesController.deleteProperty);

// Sellers Routes
router.route('/sellers').get(sellersController.sellersList);
router.route('/sellers').post(sellersController.sellersList);
router.route('/sellers/getSellerDetails/:id').get(sellersController.getSellerDetails);
router.route('/sellers/getSellerProperties/:id').get(sellersController.getSellerProperties);
router.route('/sellers/changeSellerStatus/:id/:status').get(sellersController.changeSellerStatus);
router.route('/sellers/deleteSeller/:id').get(sellersController.deleteSeller);

module.exports = router;