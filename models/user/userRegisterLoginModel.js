const { request, response } = require('express');
const db = require('../dbConnection');

var bcrypt = require('bcrypt');
// var Promise = require('promise');

const registerUser = (request, response) => {
    const { name, email, mobile_number, password, confirm_password } = request.body;
    if (!name) {
        response('Name is empty');
    }else if (!email) {
        response('Email is empty');
    }else if (!mobile_number) {
        response('Mobile Number is empty');
    }else if (!password) {
        response('Password is empty');
    }else if (password != confirm_password ){
        response('Password does not match');
    }else{
        const hashedPassword = bcrypt.hashSync(password, 10);
        db.query("INSERT INTO users (name, email, mobile_number, password, role) VALUES (?, ?, ?, ?, ?)", [name, email, mobile_number, hashedPassword, 'User'], (error, result) => {
            if (error) {
                response('Database connection error');
            } else {
                response(null, result.insertId);
            }
        });
    }
}

const loginUser = (request, response) => {
    const { email_or_mobile_number, password } = request.body;
    if (!email_or_mobile_number) {
        response('Email or Mobile Number is empty');
    }else if (!password) {
        response('Password is empty');
    }else{
        db.query("SELECT id, email, mobile_number, password FROM users where email = ? OR mobile_number = ?", [email_or_mobile_number, email_or_mobile_number], (error, result) => {
            if (error) {
                response('Database connection error');
            } else {
                if (result.length > 0) {
                    hash = (result[0].password);
                    if (bcrypt.compareSync(password, hash)) {
                        response(null, result[0].id);
                    } else {
                        response('Incorrect Password');
                    }
                } else {
                    response('No User Found');
                }
            }
        });
    }
}

const updateUser = (request, response) => {
    const { id, name, email, mobile_number } = request.body;
    if (!id) {
       response('ID Missing');
    } else if (!email) {
        response('Email is empty');
    } else if (!mobile_number) {
        response('Mobile Number is empty');
    }else {
        db.query('UPDATE users SET name = ?, email = ?, mobile_number = ? WHERE id = ?', [name, email, mobile_number, id], (error, result) => {
            if (error) {
                response('Database connection error');
            } else {
                response(null, result);
            }
        });
    }
}

const checkEmail = (request, response) => {
    const email = request.body.email;
    if (!email) {
        response('Email is empty');
    } else {
        db.query("SELECT email FROM users where email = ?", [email], (error, result) => {
            if (error) {
                response('Database connection error');
            } else {
                response(null, result);
            }
        });
    }
}

const checkMobileNumber = (request, response) => {
    const mobile_number = request.body.mobile_number;
    if (!mobile_number) {
        response('Mobile Number is empty');
    } else {
        db.query("SELECT mobile_number FROM users where mobile_number = ?", [mobile_number], (error, result) => {
            if (error) {
                response('Database connection error');
            } else {
                response(null, result);
            }
        });
    }
}

module.exports = {
    registerUser,
    loginUser,
    updateUser,
    checkEmail,
    checkMobileNumber
}