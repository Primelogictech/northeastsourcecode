const { request, response } = require('express');
const serviceSellersModel = require('../../models/admin/serviceSellersModel');

const serviceSellersList = (request, response) => {
    serviceSellersModel.serviceSellersList(request, (err, result) => {
        if (err) {
            response.status(400).send(err);
        } else {
            response.status(200).json({data: result});
        }
    });
}

const getServiceSellerDetails = (request, response) => {
    serviceSellersModel.getServiceSellerDetails(request, (err, result) => {
        if (err) {
            response.status(400).send(err);
        } else {
            response.status(200).json({data: result});
        }
    });
}

const changeServiceSellerStatus = (request, response) => {
    serviceSellersModel.changeServiceSellerStatus(request, (err, result) => {
        if (err) {
            response.status(400).send(err);
        } else {
            if (!result.affectedRows) {
                response.status(200).json({message: 'No Service Seller Found'});
            } else {
                response.status(200).json({message: 'Status Changed Successfully'});
            }
        }
    });
}

const deleteServiceSeller = (request, response) => {
    serviceSellersModel.deleteServiceSeller(request, (err, result) => {
        if (err) {
            response.status(400).send(err);
        } else {
            if (!result.affectedRows) {
                response.status(200).json({message: 'No Service Seller Found'});
            } else {
                response.status(200).json({message: 'Service Deleted Successfully'});
            }
        }
    });
}

module.exports = {
    serviceSellersList,
    getServiceSellerDetails,
    changeServiceSellerStatus,
    deleteServiceSeller
}