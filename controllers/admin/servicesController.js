const { request, response } = require('express');
const servicesModel = require('../../models/admin/servicesModel');

const servicesList = (request, response) => {
    servicesModel.servicesList(request, (err, result) => {
        if (err) {
            response.status(400).send('Something went wrong');
        } else {
            response.status(200).json({data: result});
        }
    });
}

const addService = (request, response) => {
    servicesModel.addService(request, (err, result) => {
        if (err) {
            response.status(400).send('Something went wrong');
        } else {
            response.status(200).json({message: 'Service Added Successfully', ServiceId: result});
        }
    });
}

const updateService = (request, response) => {
    servicesModel.updateService(request, (err, result) => {
        if (err) {
            response.status(400).send('Something went wrong');
        } else {
            if (!result.affectedRows) {
                response.status(200).json({message: 'No Service Found'});
            } else {
                response.status(200).json({message: 'Service Updated Successfully'});
            }
        }
    });
}

const changeServiceStatus = (request, response) => {
    servicesModel.changeServiceStatus(request, (err, result) => {
        if (err) {
            response.status(400).send('Something went wrong');
        } else {
            if (!result.affectedRows) {
                response.status(200).json({message: 'No Service Found'});
            } else {
                response.status(200).json({message: 'Status Changed Successfully'});
            }
        }
    });
}

const changeServicePriorityOrder = (request, response) => {
    servicesModel.changeServicePriorityOrder(request, (err, result) => {
        if (err) {
            response.status(400).send('Something went wrong');
        } else {
            if (!result.affectedRows) {
                response.status(200).json({message: 'No Service Found'});
            } else {
                response.status(200).json({message: 'Priority Order Changed Successfully'});
            }
        }
    });
}

const deleteService = (request, response) => {
    servicesModel.deleteService(request, (err, result) => {
        if (err) {
            response.status(400).send('Something went wrong');
        } else {
            if (!result.affectedRows) {
                response.status(200).json({message: 'No Service Found'});
            } else {
                response.status(200).json({message: 'Service Deleted Successfully'});
            }
        }
    });
}

module.exports = {
    servicesList,
    addService,
    updateService,
    changeServiceStatus,
    changeServicePriorityOrder,
    deleteService
}