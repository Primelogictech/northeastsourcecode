const { request, response } = require('express');
const db = require('../dbConnection');

const propertySubTypesList = (request, response) => {
    const { keyword, property_type_id } = request.body;
    var sql = `SELECT property_sub_types.id, property_sub_types.name AS property_sub_type_name, property_sub_types.status, property_sub_types.priority_order, property_types.name AS property_type_name FROM property_sub_types LEFT JOIN property_types ON property_sub_types.property_type_id=property_types.id where property_sub_types.status = 1`;
    if (property_type_id) {
        sql += ` AND property_sub_types.property_type_id = '${property_type_id}'`;
    }
    if (keyword) {
        sql += ` AND property_sub_types.name LIKE N'%${keyword}%'`;
    }
    sql += ` ORDER BY property_sub_types.priority_order`;
    db.query(sql,  (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const addPropertySubType = (request, response) => {
    const { property_type_id, name, priority_order } = request.body;
    if (!property_type_id) {
        response('Property Type is empty');
    } else if (!name) {
        response('Property Sub Type Name is empty');
    } else {
        db.query("INSERT INTO property_sub_types (property_type_id, name, priority_order) VALUES (?, ?, ?)", [property_type_id, name, priority_order], (error, result) => {
            if (error) {
                response('Database connection error');
            } else {
                response(null, result.insertId);
            }
        });
    }
}

const updatePropertySubType = (request, response) => {
    const { id, property_type_id, name, priority_order } = request.body;
    if (!id) {
        response('ID Missing');
    } else if (!property_type_id) {
        response('Property Type is empty');
    } else if (!name) {
        response('Property Sub Type Name is empty');
    } else {
        db.query('UPDATE property_sub_types SET property_type_id = ?, name = ?, priority_order = ? WHERE id = ?', [property_type_id, name, priority_order, id], (error, result) => {
            if (error) {
                response('Database connection error');
            } else {
                response(null, result);
            }
        });
    }
}

const changePropertySubTypeStatus = (request, response) => {
    const id = parseInt(request.params.id);
    const status = parseInt(request.params.status);
    db.query('UPDATE property_sub_types SET status = ? WHERE id = ?', [status, id], (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const changePropertySubTypePriorityOrder = (request, response) => {
    const id = parseInt(request.params.id);
    const priority_order = parseInt(request.params.priority_order);
    db.query('UPDATE property_sub_types SET priority_order = ? WHERE id = ?', [priority_order, id], (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const deletePropertySubType = (request, response) => {
    const id = parseInt(request.params.id);
    db.query('DELETE FROM property_sub_types WHERE id = ?', [id], (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

module.exports = {
    propertySubTypesList,
    addPropertySubType,
    updatePropertySubType,
    changePropertySubTypeStatus,
    changePropertySubTypePriorityOrder,
    deletePropertySubType
}