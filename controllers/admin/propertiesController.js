const { request, response } = require('express');
const propertiesModel = require('../../models/admin/propertiesModel');

const propertiesList = (request, response) => {
    propertiesModel.propertiesList(request, (err, result) => {
        if (err) {
            response.status(400).send(err);
        } else {
            response.status(200).json({data: result});
        }
    });
}

const getPropertyDetails = (request, response) => {
    propertiesModel.getPropertyDetails(request, (err, result) => {
        if (err) {
            response.status(400).send(err);
        } else {
            if (!result[0].id) {
                response.status(200).json({message: 'No Property Found'});
            } else {
                response.status(200).json({data: result});
            }
        }
    });
}

const changePropertyStatus = (request, response) => {
    propertiesModel.changePropertyStatus(request, (err, result) => {
        if (err) {
            response.status(400).send(err);
        } else {
            if (!result.affectedRows) {
                response.status(200).json({message: 'No Property Found'});
            } else {
                response.status(200).json({message: 'Status Changed Successfully'});
            }
        }
    });
}

const changeFeaturedStatus = (request, response) => {
    propertiesModel.changeFeaturedStatus(request, (err, result) => {
        if (err) {
            response.status(400).send(err);
        } else {
            if (!result.affectedRows) {
                response.status(200).json({message: 'No Property Found'});
            } else {
                response.status(200).json({message: 'Status Changed Successfully'});
            }
        }
    });
}

const deleteProperty = (request, response) => {
    propertiesModel.deleteProperty(request, (err, result) => {
        if (err) {
            response.status(400).send(err);
        } else {
            if (!result.affectedRows) {
                response.status(200).json({message: 'No Property Found'});
            } else {
                response.status(200).json({message: 'Property Deleted Successfully'});
            }
        }
    });
}

module.exports = {
    propertiesList,
    getPropertyDetails,
    changePropertyStatus,
    changeFeaturedStatus,
    deleteProperty
}