const jwt = require('jsonwebtoken');

const verifyToken = async (req, res, next) => {
    const token = req.cookies.token || '';
    if (!token) {
        res.render('admin/login', { error: 'You need to login' });
        // res.redirect('login');
    } else {
        const decrypt = await jwt.verify(token, process.env.JWT_SECRET);
        req.user = {
            userId: decrypt.userId,
            userName: decrypt.userName,
        };
        next();
    }
};

module.exports = verifyToken;