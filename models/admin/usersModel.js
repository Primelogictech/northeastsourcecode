const { request, response } = require('express');
const db = require('../dbConnection');

const usersList = (request, response) => {
    const { keyword } = request.body;
    var sql = `SELECT id, name, email, mobile_number, status FROM users WHERE`;
    if (keyword) {
        sql += ` name LIKE N'%${keyword}%' OR email LIKE N'%${keyword}%' OR mobile_number LIKE N'%${keyword}%' AND`;
    }
    sql += ` role = 'User'`;
    db.query(sql,  (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const getUserData = (request, response) => {
    const id = parseInt(request.params.id);
    db.query("SELECT name, email, mobile_number FROM users WHERE role = 'User' AND id = ?", [id], (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const changeUserStatus = (request, response) => {
    const id = parseInt(request.params.id);
    const status = parseInt(request.params.status);
    db.query('UPDATE users SET status = ? WHERE id = ?', [status, id], (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const deleteUser = (request, response) => {
    const id = parseInt(request.params.id);
    db.query('DELETE FROM users WHERE id = ?', [id], (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

// const updateUser = (request, response) => {
//     const id = parseInt(request.params.id);
//     const { name, email, mobile_number } = request.body;
//     db.query('UPDATE users SET name = $1, email = $2, mobile_number = $3 WHERE id = $4', [name, email, mobile_number, id], (error, result) => {
//         if (error) {
//             response('Database connection error');
//         } else {
//             response(null, id);
//         }
//     });
// }

module.exports = {
    usersList,
    getUserData,
    changeUserStatus,
    deleteUser
}