const { request, response } = require('express');
const db = require('../dbConnection');

const propertyTypesList = (request, response) => {
    const { keyword } = request.body;
    var sql = `SELECT * FROM property_types where status = 1`;
    if (keyword) {
        sql += ` AND name LIKE N'%${keyword}%'`;
    }
    sql += ` ORDER BY priority_order`;
    db.query(sql,  (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const addPropertyType = (request, response) => {
    const { name, priority_order } = request.body;
    if (!name) {
        response('Property Type Name is empty');
    } else {
        db.query("INSERT INTO property_types (name, priority_order) VALUES (?, ?)", [name, priority_order], (error, result) => {
            if (error) {
                response('Database connection error');
            } else {
                response(null, result.insertId);
            }
        });
    }
}

const updatePropertyType = (request, response) => {
    const { id, name, priority_order } = request.body;
    if (!id) {
        response('ID Missing');
    } else if (!name) {
        response('Property Type Name is empty');
    } else {
        db.query('UPDATE property_types SET name = ?, priority_order = ? WHERE id = ?', [name, priority_order, id], (error, result) => {
            if (error) {
                response('Database connection error');
            } else {
                response(null, result);
            }
        });
    }
}

const changePropertyTypeStatus = (request, response) => {
    const id = parseInt(request.params.id);
    const status = parseInt(request.params.status);
    db.query('UPDATE property_types SET status = ? WHERE id = ?', [status, id], (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const changePropertyTypePriorityOrder = (request, response) => {
    const id = parseInt(request.params.id);
    const priority_order = parseInt(request.params.priority_order);
    db.query('UPDATE property_types SET priority_order = ? WHERE id = ?', [priority_order, id], (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const deletePropertyType = (request, response) => {
    const id = parseInt(request.params.id);
    db.query('DELETE FROM property_types WHERE id = ?', [id], (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

module.exports = {
    propertyTypesList,
    addPropertyType,
    updatePropertyType,
    changePropertyTypeStatus,
    changePropertyTypePriorityOrder,
    deletePropertyType
}