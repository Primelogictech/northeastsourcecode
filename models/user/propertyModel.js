const { request, response } = require('express');
const db = require('../dbConnection');
var path = require('path');
const { extname } = require('path');

const uploadProperty = (request, response) => {
    const { user_id, mobile_number_visibility, alt_mobile_number, alt_mobile_number_visibility, seller_type, available_for, property_title, property_type_id, property_sub_type_id, bhk_type, gated_security, floor, total_floors, property_age, facing, property_size, property_size_type, society_name, price_type, price, price_range_to, property_description, address_line_1, address_line_2, city, state, country, available_amenities } = request.body;
    if (user_id && alt_mobile_number && seller_type && available_for && property_title && property_type_id && property_sub_type_id && bhk_type && gated_security && floor && total_floors && property_age && facing && property_size && property_size_type && society_name && price_type && price && property_description && address_line_1 && city && state && country && available_amenities) {
        db.query("SELECT id FROM users WHERE id = ?", [user_id], (error, result) => {
            if (error) {
                response('Database connection error');
            } else {
                if (result.length > 0) {
                    let floor_plan = "";
                    let property_images = "";
                    if (request.files) {
                        let floorPlanImage;
                        let floorPlanImagePath;
                        let floorPlanImagesArray = [];
                        if (request.files.floor_plan.length > 1) {
                            request.files.floor_plan.forEach(element => {
                                var timestamp = Date.now();
                                var randomString = ("" + Math.random()).substring(2, 8);
                                var floorPlanImageName = timestamp + randomString + path.extname(element.name);
                                floorPlanImagePath = path.join(__dirname, '../../uploads/') + floorPlanImageName;
                                element.mv(floorPlanImagePath);
                                floorPlanImagesArray.push(floorPlanImageName);
                            });
                        } else {
                            floorPlanImage = request.files.floor_plan;
                            var timestamp = Date.now();
                            var randomString = ("" + Math.random()).substring(2, 8);
                            var floorPlanImageName = timestamp + randomString + path.extname(floorPlanImage.name);
                            floorPlanImagePath = path.join(__dirname, '../../uploads/') + floorPlanImageName;
                            floorPlanImage.mv(floorPlanImagePath);
                            floorPlanImagesArray.push(floorPlanImageName);
                        }
                        floor_plan = floorPlanImagesArray.join(', ');
                    } else {
                        return response('Floor Plan is missing');
                    }
                    
                    if (request.files) {
                        let propertyImage;
                        let propertyImagePath;
                        let propertyImagesArray = [];
                        if (request.files.property_images.length > 1) {
                            request.files.property_images.forEach(element => {
                                var timestamp = Date.now();
                                var randomString = ("" + Math.random()).substring(2, 8);
                                var propertyImageName = timestamp + randomString + path.extname(element.name);
                                propertyImagePath = path.join(__dirname, '../../uploads/') + propertyImageName;
                                element.mv(propertyImagePath);
                                propertyImagesArray.push(propertyImageName);
                            });
                        } else {
                            propertyImage = request.files.property_images;
                            var timestamp = Date.now();
                            var randomString = ("" + Math.random()).substring(2, 8);
                            var propertyImageName = timestamp + randomString + path.extname(propertyImage.name);
                            propertyImagePath = path.join(__dirname, '../../uploads/') + propertyImageName;
                            propertyImage.mv(propertyImagePath);
                            propertyImagesArray.push(propertyImageName);
                        }
                        property_images = propertyImagesArray.join(', ');
                    } else {
                        return response('Property Images are missing');
                    }

                    db.query("INSERT INTO properties (user_id, seller_type, available_for, property_title, property_type_id, property_sub_type_id, bhk_type, gated_security, floor, total_floors, property_age, facing, property_size, property_size_type, society_name, price_type, price, price_range_to, property_description, floor_plan, property_images, address_line_1, address_line_2, city, state, country, available_amenities) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [user_id, seller_type, available_for, property_title, property_type_id, property_sub_type_id, bhk_type, gated_security, floor, total_floors, property_age, facing, property_size, property_size_type, society_name, price_type, price, price_range_to, property_description, floor_plan, property_images, address_line_1, address_line_2, city, state, country, available_amenities], (error, result) => {
                        if (error) {
                            response('Database connection error');
                        } else {
                            if (result.insertId) {
                                propertyId = result.insertId;
                                db.query('UPDATE users SET mobile_number_visibility = ?, alt_mobile_number = ?, alt_mobile_number_visibility = ?, is_seller = 1 WHERE id = ?', [mobile_number_visibility, alt_mobile_number, alt_mobile_number_visibility, user_id], (error, result) => {
                                    if (error) {
                                        response('Database connection error');
                                    } else {
                                        response(null, propertyId);
                                    }
                                });
                            } else {
                                response('Something went wrong');
                            }
                        }
                    });
                } else {
                    response('No User Found');
                }
            }
        });
    } else {
        response('Please input all fields');
    }
}

const propertiesList = (request, response) => {
    const { keyword, property_type_id, property_sub_type_id } = request.body;
    var sql = `SELECT p.*, pt.name as property_type, pst.name as property_sub_type, GROUP_CONCAT(amenities.name) AS amenities FROM properties AS p LEFT JOIN property_types AS pt ON p.property_type_id = pt.id LEFT JOIN property_sub_types AS pst ON p.property_sub_type_id = pst.id LEFT JOIN amenities ON FIND_IN_SET(amenities.id, p.available_amenities) WHERE`;
    if (keyword) {
        sql += ` p.property_title LIKE N'%${keyword}%' OR p.society_name LIKE N'%${keyword}%' OR p.city LIKE N'%${keyword}%' AND`;
    }
    if (property_type_id) {
        sql += ` p.property_type_id = '${property_type_id}' AND`;
    }
    if (property_sub_type_id) {
        sql += ` p.property_sub_type_id = '${property_sub_type_id}' AND`;
    }
    sql += ` p.status = 1 GROUP BY p.id ORDER BY p.id DESC`;
    db.query(sql,  (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const addToShortList = (request, response) => {
    const property_id = parseInt(request.body.property_id);
    const user_id = parseInt(request.body.user_id);
    if (property_id && user_id) {
        db.query("SELECT * FROM short_listed_properties WHERE user_id = ? AND property_id = ?", [user_id, property_id], (error, res) => {
            if (error) {
                response('Database connection error');
            } else {
                if (res && res.length > 0) {
                    response(null, 'Already short listed');
                } else {
                    db.query('INSERT INTO short_listed_properties (user_id, property_id) VALUES (?, ?)', [user_id, property_id], (error, result) => {
                        if (error) {
                            response('Database connection error');
                        } else {
                            response(null, 'Property added to short list');
                        }
                    });
                }
            }
        });
    }
}

const removeFromShortList = (request, response) => {
    const property_id = parseInt(request.body.property_id);
    const user_id = parseInt(request.body.user_id);
    if (property_id && user_id) {
        db.query("DELETE FROM short_listed_properties WHERE user_id = ? AND property_id = ?", [user_id, property_id], (error, result) => {
            if (error) {
                response('Database connection error');
            } else {
                response(null, 'Property removed from short list');
            }
        });
    }
}

const addToRecentlyViewed = (request, response) => {
    const property_id = parseInt(request.body.property_id);
    const user_id = parseInt(request.body.user_id);
    if (property_id && user_id) {
        db.query("SELECT * FROM recently_viewed_properties WHERE user_id = ? AND property_id = ?", [user_id, property_id], (error, res) => {
            if (error) {
                response('Database connection error');
            } else {
                if (res && res.length > 0) {
                    response(null, 'Already added to recently viewed list');
                } else {
                    db.query('INSERT INTO recently_viewed_properties (user_id, property_id) VALUES (?, ?)', [user_id, property_id], (error, result) => {
                        if (error) {
                            response('Database connection error');
                        } else {
                            response(null, 'Property added to recently viewed list');
                        }
                    });
                }
            }
        });
    }
}

module.exports = {
    uploadProperty,
    propertiesList,
    addToShortList,
    removeFromShortList,
    addToRecentlyViewed
}