const { request, response } = require('express');
const db = require('../dbConnection');
const fs = require('fs');
var path = require('path');
const { extname } = require('path');

const servicesList = (request, response) => {
    const { keyword } = request.body;
    var sql = `SELECT * FROM services where status = 1`;
    if (keyword) {
        sql += ` AND name LIKE N'%${keyword}%'`;
    }
    sql += ` ORDER BY priority_order`;
    db.query(sql,  (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const addService = (request, response) => {
    const { name, priority_order } = request.body;
    if (!name) {
        response('Service Name is empty');
    } else {
        if (request.files.image) {
            sampleFile = request.files.image;
            var timestamp = Date.now();
            var randomString = ("" + Math.random()).substring(2, 8);
            var fileName = timestamp + randomString + path.extname(sampleFile.name);
            uploadPath = path.join(__dirname, '../../uploads/') + fileName;
            sampleFile.mv(uploadPath);
            const image = fileName;

            db.query("INSERT INTO services (name, image, priority_order) VALUES (?, ?, ?)", [name, image, priority_order], (error, result) => {
                if (error) {
                    response('Database connection error');
                } else {
                    response(null, result.insertId);
                }
            });
        } else {
            return response('Service Image is required');
        }
    }
}

const updateService = (request, response) => {
    const { id, name, priority_order } = request.body;
    if (!id) {
        response('ID Missing');
    } else if (!name) {
        response('Service Name is empty');
    } else {
        if (request.files.image) {
            db.query('SELECT image FROM services where id = ?', [id], (error, result) => {
                if (error) {
                    response('Database connection error');
                } else {
                    if (result) {
                        if (result[0].image) {
                            var image_path = path.join(__dirname, '../../uploads/') + result[0].image;
                            fs.unlink(image_path, (err) => {
                                if (err) {
                                    response(err);
                                }
                            });
                        }
                    }
                }
            });
            sampleFile = request.files.image;
            var timestamp = Date.now();
            var randomString = ("" + Math.random()).substring(2, 8);
            var fileName = timestamp + randomString + path.extname(sampleFile.name);
            uploadPath = path.join(__dirname, '../../uploads/') + fileName;
            sampleFile.mv(uploadPath);
            const image = fileName;

            db.query('UPDATE services SET name = ?, image = ?, priority_order = ? WHERE id = ?', [name, image, priority_order, id], (error, result) => {
                if (error) {
                    response('Database connection error');
                } else {
                    response(null, result);
                }
            });
        } else {
            db.query('UPDATE services SET name = ?, priority_order = ? WHERE id = ?', [name, priority_order, id], (error, result) => {
                if (error) {
                    response('Database connection error');
                } else {
                    response(null, result);
                }
            });
        }
    }
}

const changeServiceStatus = (request, response) => {
    const id = parseInt(request.params.id);
    const status = parseInt(request.params.status);
    db.query('UPDATE services SET status = ? WHERE id = ?', [status, id], (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const changeServicePriorityOrder = (request, response) => {
    const id = parseInt(request.params.id);
    const priority_order = parseInt(request.params.priority_order);
    db.query('UPDATE services SET priority_order = ? WHERE id = ?', [priority_order, id], (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const deleteService = (request, response) => {
    const id = parseInt(request.params.id);
    if (id) {
        db.query('SELECT image FROM services where id = ?', [id], (error, result) => {
            if (error) {
                response('Database connection error');
            } else {
                if (result) {
                    if (result[0].image) {
                        var image_path = path.join(__dirname, '../../uploads/') + result[0].image;
                        fs.unlink(image_path, (err) => {
                            if (err) {
                                response(err);
                            }
                        });
                    }
                }
            }
        });
        db.query('DELETE FROM services WHERE id = ?', [id], (error, result) => {
            if (error) {
                response('Database connection error');
            } else {
                response(null, result);
            }
        });
    } else {
        response('ID Missing');
    }
}

module.exports = {
    servicesList,
    addService,
    updateService,
    changeServiceStatus,
    changeServicePriorityOrder,
    deleteService
}