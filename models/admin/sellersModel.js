const { request, response } = require('express');
const db = require('../dbConnection');

const sellersList = (request, response) => {
    const { keyword } = request.body;
    var sql = `SELECT id, name, email, mobile_number, alt_mobile_number, status FROM users WHERE`;
    if (keyword) {
        sql += ` ( name LIKE N'%${keyword}%' OR email LIKE N'%${keyword}%' OR mobile_number LIKE N'%${keyword}%' ) AND`;
    }
    sql += ` role = 'User' AND is_seller = 1`;
    db.query(sql,  (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const getSellerDetails = (request, response) => {
    const id = parseInt(request.params.id);
    db.query("SELECT name, email, mobile_number, alt_mobile_number, mobile_number_visibility, alt_mobile_number_visibility, status, created_at, updated_at FROM users WHERE id = ?", [id], (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const getSellerProperties = (request, response) => {
    const id = parseInt(request.params.id);
    db.query("SELECT p.*, GROUP_CONCAT(amenities.name) AS amenities FROM properties AS p LEFT JOIN amenities ON FIND_IN_SET(amenities.id, p.available_amenities) WHERE p.user_id = ? GROUP BY p.id", [id], (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const changeSellerStatus = (request, response) => {
    const id = parseInt(request.params.id);
    const status = parseInt(request.params.status);
    db.query('UPDATE users SET status = ? WHERE id = ?', [status, id], (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const deleteSeller = (request, response) => {
    const id = parseInt(request.params.id);
    db.query('DELETE FROM users WHERE id = ?', [id], (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

module.exports = {
    sellersList,
    getSellerDetails,
    getSellerProperties,
    changeSellerStatus,
    deleteSeller
}