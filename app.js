var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var logger = require('morgan');
const fileUpload = require('express-fileupload');
const helmet = require("helmet");
require('dotenv').config();
var indexRouter = require('./routes/index');
var adminRouter = require('./routes/admin');
var cors = require('cors');

var app = express();
var sessionConfig = {
  secret: "mr1WB0OQePIxuFZxdf08OhWOcf1SyWF4",
  resave: false,
  saveUninitialized: true,
  cookie: {
    maxAge: (60 * 60 * 1000)
  }
};
app.use(session(sessionConfig));
app.use(cors());
// app.locals.baseUrl = "http://localhost:3000/";
//app.locals.baseUrl = "http://3.19.238.206:80/";
app.locals.baseUrl = "http://nagoodu.com/";

app.use(helmet()); // helps you secure your Express apps by setting various HTTP requests

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(fileUpload({ safeFileNames: true, preserveExtension: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'uploads')));

app.use(function(req, res, next) {
  if (!req.user)
    res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
  next();
});
app.use('/', (req, res, next) => {
  res.locals.userId = req.session.userId;
  res.locals.isUserServiceSeller = req.session.isUserServiceSeller;
  res.set("Content-Security-Policy", "default-src *; style-src 'self' http://* 'unsafe-inline'; img-src * data: blob: 'unsafe-inline'; script-src 'self' http://* 'unsafe-inline' 'unsafe-eval'");
  next();
}, indexRouter);
app.use('/admin', (req, res, next) => {
  res.set("Content-Security-Policy", "default-src *; style-src 'self' http://* 'unsafe-inline'; img-src * data: blob: 'unsafe-inline'; script-src 'self' http://* 'unsafe-inline' 'unsafe-eval'");
  next();
}, adminRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
