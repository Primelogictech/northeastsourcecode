const { request, response } = require('express');
const propertySubTypesModel = require('../../models/admin/propertySubTypesModel');

const propertySubTypesList = (request, response) => {
    propertySubTypesModel.propertySubTypesList(request, (err, result) => {
        if (err) {
            response.status(400).send('Something went wrong');
        } else {
            response.status(200).json({data: result});
        }
    });
}

const addPropertySubType = (request, response) => {
    propertySubTypesModel.addPropertySubType(request, (err, result) => {
        if (err) {
            response.status(400).send('Something went wrong');
        } else {
            response.status(200).json({message: 'Property Sub Type Added Successfully', propertySubTypeId: result});
        }
    });
}

const updatePropertySubType = (request, response) => {
    propertySubTypesModel.updatePropertySubType(request, (err, result) => {
        if (err) {
            response.status(400).send('Something went wrong');
        } else {
            if (!result.affectedRows) {
                response.status(200).json({message: 'No Property Sub Type Found'});
            } else {
                response.status(200).json({message: 'Property Sub Type Updated Successfully'});
            }
        }
    });
}

const changePropertySubTypeStatus = (request, response) => {
    propertySubTypesModel.changePropertySubTypeStatus(request, (err, result) => {
        if (err) {
            response.status(400).send('Something went wrong');
        } else {
            if (!result.affectedRows) {
                response.status(200).json({message: 'No Property Sub Type Found'});
            } else {
                response.status(200).json({message: 'Status Changed Successfully'});
            }
        }
    });
}

const changePropertySubTypePriorityOrder = (request, response) => {
    propertySubTypesModel.changePropertySubTypePriorityOrder(request, (err, result) => {
        if (err) {
            response.status(400).send('Something went wrong');
        } else {
            if (!result.affectedRows) {
                response.status(200).json({message: 'No Property Sub Type Found'});
            } else {
                response.status(200).json({message: 'Priority Order Changed Successfully'});
            }
        }
    });
}

const deletePropertySubType = (request, response) => {
    propertySubTypesModel.deletePropertySubType(request, (err, result) => {
        if (err) {
            response.status(400).send('Something went wrong');
        } else {
            if (!result.affectedRows) {
                response.status(200).json({message: 'No Property Sub Type Found'});
            } else {
                response.status(200).json({message: 'Property Sub Type Deleted Successfully'});
            }
        }
    });
}

module.exports = {
    propertySubTypesList,
    addPropertySubType,
    updatePropertySubType,
    changePropertySubTypeStatus,
    changePropertySubTypePriorityOrder,
    deletePropertySubType
}