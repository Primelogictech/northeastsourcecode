-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 25, 2021 at 07:56 PM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 7.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `real_estate`
--

-- --------------------------------------------------------

--
-- Table structure for table `amenities`
--

CREATE TABLE `amenities` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `priority_order` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `amenities`
--

INSERT INTO `amenities` (`id`, `name`, `status`, `priority_order`) VALUES
(1, 'Lift', 1, 0),
(2, 'Air Conditioner', 1, 0),
(12, 'Fire Safety', 1, 0),
(16, 'Power Backup', 1, 0),
(17, 'Watchman-Security', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `otp`
--

CREATE TABLE `otp` (
  `id` int(11) NOT NULL,
  `mobile_number` varchar(20) NOT NULL,
  `otp` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `otp`
--

INSERT INTO `otp` (`id`, `mobile_number`, `otp`) VALUES
(1, '9052304477', '12345'),
(2, '8639930690', '12345'),
(3, '8555941143', '12345'),
(4, '88018 56911 ', '12345'),
(5, '8801856911', '12345'),
(6, '9700845769', '12345');

-- --------------------------------------------------------

--
-- Table structure for table `properties`
--

CREATE TABLE `properties` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `seller_type` varchar(20) NOT NULL,
  `available_for` int(11) NOT NULL COMMENT '1=>for sale, 2=>for rent, 3=>for lease, 4=>all',
  `property_title` varchar(255) NOT NULL,
  `property_type_id` int(11) NOT NULL,
  `property_sub_type_id` int(11) NOT NULL,
  `bhk_type` int(11) NOT NULL,
  `gated_security` varchar(50) NOT NULL,
  `floor` int(11) NOT NULL,
  `total_floors` int(11) NOT NULL,
  `property_age` int(11) NOT NULL,
  `facing` varchar(100) NOT NULL,
  `property_size` varchar(50) NOT NULL,
  `property_size_type` int(11) NOT NULL COMMENT '1=>square feets, 2=>acres',
  `society_name` varchar(255) NOT NULL,
  `price_type` int(11) NOT NULL COMMENT '1=>fixed, 2=>starting from, 3=>range',
  `price` int(11) NOT NULL COMMENT 'fixed price, price starting from, price range from',
  `price_range_to` int(11) DEFAULT NULL COMMENT 'price range to',
  `property_status_id` int(11) NOT NULL,
  `property_description` text NOT NULL,
  `floor_plan` varchar(255) NOT NULL,
  `property_images` varchar(255) NOT NULL,
  `address_line_1` text NOT NULL,
  `address_line_2` text DEFAULT NULL,
  `city` varchar(255) NOT NULL,
  `pincode` varchar(10) NOT NULL,
  `state` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `latitude` varchar(50) DEFAULT NULL,
  `longitude` varchar(50) DEFAULT NULL,
  `available_amenities` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `is_featured` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `property_status_values`
--

CREATE TABLE `property_status_values` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `priority_order` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `property_status_values`
--

INSERT INTO `property_status_values` (`id`, `name`, `status`, `priority_order`) VALUES
(1, 'Furnished, Immediately Available For Sale', 1, 1),
(2, 'Unfurnished, Immediately Available For Sale', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `property_sub_types`
--

CREATE TABLE `property_sub_types` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `property_type_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `priority_order` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `property_sub_types`
--

INSERT INTO `property_sub_types` (`id`, `name`, `property_type_id`, `status`, `priority_order`) VALUES
(8, 'Shops', 1, 1, 1),
(9, 'Buildings', 1, 1, 2),
(10, 'Flat', 1, 1, 3),
(11, 'Open Plot', 1, 1, 4),
(12, 'Commercial Plot', 5, 1, 1),
(13, 'Independent Plot', 5, 1, 2),
(14, 'Independent House Or Villa', 4, 1, 1),
(15, 'Flat', 4, 1, 2),
(16, 'Barren Land', 2, 1, 2),
(17, 'Fertile Land', 2, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `property_types`
--

CREATE TABLE `property_types` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `priority_order` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `property_types`
--

INSERT INTO `property_types` (`id`, `name`, `status`, `priority_order`) VALUES
(1, 'Commercial Property', 1, 1),
(2, 'Farm Land - Agriculture Land', 1, 4),
(4, 'Residential Property', 1, 2),
(5, 'Plot', 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `recently_viewed_properties`
--

CREATE TABLE `recently_viewed_properties` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `property_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `priority_order` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `name`, `image`, `status`, `priority_order`) VALUES
(1, 'Plumbing Services', '1628599088847810843.png', 1, 1),
(2, 'Electrician Services', '1628599244128890323.png', 1, 2),
(3, 'Painting Services', '1628599258307443163.png', 1, 3),
(4, 'Carpenter Services', '1628599269563822730.png', 1, 4),
(5, 'Tiles Work Service', '1628599279043605072.png', 1, 5),
(6, 'Building Material', '1628599288296918933.png', 1, 6),
(7, 'Water Service', '1628599317010146735.png', 1, 7),
(8, 'Property Registration', '1628599356621951055.png', 1, 8),
(9, 'Interior Services', '1630268223797589095.png', 1, 9),
(10, 'Property Management Services', '1630339835221994723.jpg', 1, 6);

-- --------------------------------------------------------

--
-- Table structure for table `service_bookings`
--

CREATE TABLE `service_bookings` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `service_seller_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `service_bookings`
--

INSERT INTO `service_bookings` (`id`, `user_id`, `service_id`, `service_seller_id`, `status`, `created_at`) VALUES
(1, 59, 1, 59, 0, '2021-08-29 17:58:44'),
(2, 64, 1, 59, 0, '2021-10-11 07:29:35');

-- --------------------------------------------------------

--
-- Table structure for table `service_sellers`
--

CREATE TABLE `service_sellers` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `business_name` varchar(255) NOT NULL,
  `business_experience` int(11) NOT NULL,
  `address` text NOT NULL,
  `tags` int(11) NOT NULL,
  `about` text NOT NULL,
  `images` text NOT NULL,
  `opening_time` time NOT NULL,
  `closing_time` time NOT NULL,
  `services_offered` text NOT NULL,
  `joined_on` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `service_sellers`
--

INSERT INTO `service_sellers` (`id`, `user_id`, `business_name`, `business_experience`, `address`, `tags`, `about`, `images`, `opening_time`, `closing_time`, `services_offered`, `joined_on`) VALUES
(1, 60, 'Messi Services', 8, 'Hitech City, Hyderabad', 1, 'All electrical services are available', '1630247534258935700.jpg', '10:00:00', '18:00:00', '7', '2021-08-29 14:32:14'),
(2, 59, 'Pavan Traders', 10, 'Karimanagar', 1, 'We are expert in plumbing services', '1630259893872765640.jpg', '19:07:00', '06:06:00', '1', '2021-08-29 17:58:13');

-- --------------------------------------------------------

--
-- Table structure for table `short_listed_properties`
--

CREATE TABLE `short_listed_properties` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `property_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `priority_order` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `name`, `status`, `priority_order`) VALUES
(1, 'Electrician', 1, 1),
(2, 'Plumber', 1, 2),
(3, 'Carpenter', 1, 3),
(4, 'Painter', 1, 4);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `mobile_number` varchar(20) NOT NULL,
  `alt_mobile_number` varchar(20) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `about` text DEFAULT NULL,
  `profile_picture` varchar(255) DEFAULT NULL,
  `role` varchar(30) NOT NULL,
  `is_seller` int(11) NOT NULL DEFAULT 0,
  `is_service_seller` int(11) NOT NULL DEFAULT 0,
  `mobile_number_visibility` int(11) NOT NULL DEFAULT 1,
  `alt_mobile_number_visibility` int(11) NOT NULL DEFAULT 1,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `mobile_number`, `alt_mobile_number`, `password`, `about`, `profile_picture`, `role`, `is_seller`, `is_service_seller`, `mobile_number_visibility`, `alt_mobile_number_visibility`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@gmail.com', '0123456789', '0123456789', '$2b$10$4tJerKJX3z16ZpE7z5mOSOaXEZtByr3qR5Wa.I0MGJS1r0lhW7wPi', 'Owner selling personal property', NULL, 'Admin', 1, 0, 0, 0, 1, '2021-07-06 07:14:29', '2021-10-18 21:27:58'),
(2, 'User', 'user@gmail.com', '9848012345', '9848012345', '$2b$10$nAHm..gt.nDmqTvceLAzO.wPrqzZNYYs4ihRKcHSmIYZi2G8VArd2', 'I am User.  This is about me.', NULL, 'User', 1, 0, 0, 0, 1, '2021-07-06 10:10:56', '2021-10-19 10:22:50'),
(3, 'Pawan Kalyan', 'pspk@gmail.com', '9876543210', '9848012345', '$2b$10$dNIhSxuiBN.Wd.gtmqXLFudIkk7NCxeT2wqb/d6tLW3O.2PdrW8NG', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', NULL, 'User', 0, 0, 0, 0, 1, '2021-07-06 10:11:27', '2021-08-27 13:47:39'),
(4, 'Mahesh Babu', 'ssmb@gmail.com', '9012345678', NULL, '$2b$10$Ui8MZYaOhNRrfTNrPu1VsuyVLjnzy0UEIAkJZgdj1Q708r4.ju6/a', NULL, NULL, 'User', 0, 0, 1, 1, 1, '2021-07-06 10:12:34', NULL),
(5, 'NTR', 'ntr@gmail.com', '9876543211', NULL, '$2b$10$O1pDUSJ6MwhNe61Hx3l42us/8QQF7Jjskm6Ndg3xHBQWe8PnZ859i', NULL, NULL, 'User', 0, 0, 1, 1, 1, '2021-07-06 14:20:05', NULL),
(6, 'Allu Arjun', 'aa@gmail.com', '9876543212', NULL, '$2b$10$fvOZ5T/4t/m9e87PouSpr.cIwfryL.8mvbUMwPohqSey9xnWoErqC', NULL, NULL, 'User', 0, 0, 1, 1, 1, '2021-07-07 07:22:35', NULL),
(7, 'Test', 'test@gmail.com', '9999999999', NULL, '$2b$10$eqFnbfkKdsJ1hcLLHfwmZe5yyn4jC4cZwZ3SypZhCvZ.Jk36nGV4a', NULL, NULL, 'User', 0, 0, 1, 1, 1, '2021-07-08 11:25:42', '2021-08-02 13:43:36'),
(8, 'King', 'king@gmail.com', '9848012346', '9012345678', '$2b$10$IWNtydSYDl69daVRZkw7.OKPOxYbUHVjlofVTInpT0Esd1gVtoPfa', NULL, NULL, 'User', 0, 0, 1, 1, 1, '2021-07-14 15:37:56', '2021-08-27 13:47:39'),
(49, 'Mahesh Service Seller', 'mb_service_seller@gmail.com', '9848012348', NULL, '$2b$10$SzHyupsHVxeauWGciirMMeUgDsLRZJynaZ1AbwjslqkCRxdHE7ALe', NULL, NULL, 'Service Seller', 0, 0, 1, 1, 1, '2021-07-20 14:00:23', '2021-08-27 13:47:39'),
(50, 'Service Seller', 'service_seller@gmail.com', '9848012347', NULL, '$2b$10$2lPVPZeiivVz5M3h/FGi8O/3W14VjvCNgb.kNBkOZ9QvJumCFjF1m', NULL, NULL, 'Service Seller', 0, 0, 1, 1, 1, '2021-07-20 14:01:29', '2021-08-27 13:47:39'),
(51, 'Ronaldo Service Seller', 'cr7_service_seller@gmail.com', '9848012349', NULL, '$2b$10$usA.Z4YmI14yyLFd8ZsWR.ymrH2lRLbPnQ9GDSeinhE3tlLCx1.NG', NULL, NULL, 'Service Seller', 0, 0, 1, 1, 1, '2021-07-21 05:52:15', '2021-08-27 13:47:39'),
(52, 'Test Service Seller', 'test_service_seller@gmail.com', '9848012350', NULL, '$2b$10$VyaKAdIYcM4Oxju57OL1V.0ujAwBK41/8ieKanMNOUAq3t3UHzslu', NULL, NULL, 'Service Seller', 0, 0, 1, 1, 1, '2021-07-21 14:56:39', '2021-08-27 13:47:39'),
(53, 'Chiru', 'megastar@gmail.com', '9848012351', NULL, '$2b$10$mgUtqELwtGazYnBY2HI5DOnsEwkr71zSFpAoxOIEE71rtRh9B3kmq', NULL, NULL, 'User', 0, 0, 1, 1, 1, '2021-08-10 04:25:14', NULL),
(54, 'Mahesh', 'mb2@gmail.com', '9848012352', NULL, '$2b$10$2vlYI5L4oTklY0Ln2HrtNuAoJomt1AEYYlcGEBq2CWZtj3FR3GJ4i', NULL, NULL, 'User', 0, 0, 1, 1, 1, '2021-08-23 15:50:34', '2021-08-27 13:47:39'),
(57, 'Virat', 'vk@gmail.com', '9848012353', '9848012345', '$2b$10$Ujrhr.11RVyTJekUDkF56uZJDWwBOqGShYLEoIx.8z6LmyhBa4/Ii', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. \r\n\r\nDuis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', NULL, 'Service Seller', 0, 0, 0, 0, 1, '2021-08-24 05:12:41', '2021-08-27 13:47:39'),
(58, 'Vijay Devarakonda', 'vd@gmail.com', '9848012354', NULL, '$2b$10$nLl2MFBrjhmffpWC139NNeaaklVoUpZ.eLBcfvAWAYjGRiD7zUU.i', NULL, NULL, 'Service Seller', 0, 0, 1, 1, 1, '2021-08-24 14:33:37', '2021-08-27 13:47:39'),
(59, 'Arun', 'arun.e@primelogictech.com', '9052304477', '8639930690', '$2b$10$/qnAT2O9O21PcBiVBbnnneMBtaHNnfDlGRpzN1EecXi57IRDKDuWm', 'I am the owner of the property', NULL, 'User', 1, 1, 0, 0, 1, '2021-08-29 08:59:35', '2021-08-29 19:32:22'),
(60, 'Messi', 'messi@gmail.com', '9848012355', NULL, '$2b$10$lNFKE5Rg5uubn60rOCdqjeUbnREuGRmZJHHuCJzvFicFA9KTXFiJi', NULL, NULL, 'Service Seller', 0, 1, 1, 1, 1, '2021-08-29 14:32:14', NULL),
(61, 'Kiran', 'kiran@primelogictech.com', '8639930690', NULL, '$2b$10$CwlKRvMeo5GUvWJ7pX4UUOyKMtCar2REUtvdt8G8q7swsdU1TECnG', NULL, NULL, 'User', 0, 0, 1, 1, 1, '2021-08-30 14:01:31', NULL),
(62, 'Arun', 'arunadfstuff@gmail.com', '9177852662', '8639930690', '$2b$10$xHssJ1VVrIMfzNR6i1jPWO4/lM4HJ0QimtyS/oj6ME3eLmYyh/G4.', 'I am a owner of the property', NULL, 'User', 1, 0, 0, 0, 1, '2021-08-30 15:49:51', '2021-08-30 16:06:32'),
(63, 'Jamal', 'jamaligene@gmail.com', '8555941143', '9000817982', '$2b$10$0kWWjWLMTXCniS72jL/.cOVVR0hyDIFhJxVGBI1911TJG2Rs1QmCe', 'Test Owner Property 1', NULL, 'User', 1, 0, 0, 0, 1, '2021-09-03 06:27:32', '2021-09-03 06:43:33'),
(64, 'North East Properties', 'northeastproperties@gmail.com', '8801856911', '8801856911', '$2b$10$md6pWtKc4vBtwqwukaUAsO8H1PEEtD07DFxLBm7WvNktnIbm1ofNK', 'Northeast properties & Infradevelopers ', NULL, 'User', 1, 0, 0, 0, 1, '2021-10-09 13:59:46', '2021-10-09 14:48:57'),
(65, 'Arun', 'arun@gmail.com', '9700845769', NULL, '$2b$10$buDs7JRLgGhsir590Xyuxu8ABNm9KToU1beEd7aEsKMJPxJPGbr1W', NULL, NULL, 'User', 0, 0, 1, 1, 1, '2021-10-11 10:50:15', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `amenities`
--
ALTER TABLE `amenities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `otp`
--
ALTER TABLE `otp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `properties`
--
ALTER TABLE `properties`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `properties_ibfk_2` (`property_type_id`),
  ADD KEY `property_sub_type_id` (`property_sub_type_id`),
  ADD KEY `property_status_id` (`property_status_id`);

--
-- Indexes for table `property_status_values`
--
ALTER TABLE `property_status_values`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `property_sub_types`
--
ALTER TABLE `property_sub_types`
  ADD PRIMARY KEY (`id`),
  ADD KEY `property_type_id` (`property_type_id`);

--
-- Indexes for table `property_types`
--
ALTER TABLE `property_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recently_viewed_properties`
--
ALTER TABLE `recently_viewed_properties`
  ADD PRIMARY KEY (`id`),
  ADD KEY `property_id` (`property_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_bookings`
--
ALTER TABLE `service_bookings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_id` (`service_id`),
  ADD KEY `service_seller_id` (`service_seller_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `service_sellers`
--
ALTER TABLE `service_sellers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `tags` (`tags`);

--
-- Indexes for table `short_listed_properties`
--
ALTER TABLE `short_listed_properties`
  ADD PRIMARY KEY (`id`),
  ADD KEY `property_id` (`property_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `amenities`
--
ALTER TABLE `amenities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `otp`
--
ALTER TABLE `otp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `properties`
--
ALTER TABLE `properties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `property_status_values`
--
ALTER TABLE `property_status_values`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `property_sub_types`
--
ALTER TABLE `property_sub_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `property_types`
--
ALTER TABLE `property_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `recently_viewed_properties`
--
ALTER TABLE `recently_viewed_properties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `service_bookings`
--
ALTER TABLE `service_bookings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `service_sellers`
--
ALTER TABLE `service_sellers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `short_listed_properties`
--
ALTER TABLE `short_listed_properties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `properties`
--
ALTER TABLE `properties`
  ADD CONSTRAINT `properties_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `properties_ibfk_2` FOREIGN KEY (`property_type_id`) REFERENCES `property_types` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `properties_ibfk_3` FOREIGN KEY (`property_sub_type_id`) REFERENCES `property_sub_types` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `properties_ibfk_4` FOREIGN KEY (`property_status_id`) REFERENCES `property_status_values` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `property_sub_types`
--
ALTER TABLE `property_sub_types`
  ADD CONSTRAINT `property_sub_types_ibfk_1` FOREIGN KEY (`property_type_id`) REFERENCES `property_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `recently_viewed_properties`
--
ALTER TABLE `recently_viewed_properties`
  ADD CONSTRAINT `recently_viewed_properties_ibfk_1` FOREIGN KEY (`property_id`) REFERENCES `properties` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `recently_viewed_properties_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `service_bookings`
--
ALTER TABLE `service_bookings`
  ADD CONSTRAINT `service_bookings_ibfk_1` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `service_bookings_ibfk_2` FOREIGN KEY (`service_seller_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `service_bookings_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `service_sellers`
--
ALTER TABLE `service_sellers`
  ADD CONSTRAINT `service_sellers_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `service_sellers_ibfk_2` FOREIGN KEY (`tags`) REFERENCES `tags` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `short_listed_properties`
--
ALTER TABLE `short_listed_properties`
  ADD CONSTRAINT `short_listed_properties_ibfk_1` FOREIGN KEY (`property_id`) REFERENCES `properties` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `short_listed_properties_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
