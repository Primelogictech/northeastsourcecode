const jwt = require('jsonwebtoken');
//import jwt from 'jsonwebtoken'; 

const generateToken = (res, userId, userName) => {
  const token = jwt.sign({ userId, userName }, process.env.JWT_SECRET, {
    expiresIn: '1h',
  });
  return res.cookie('token', token, {
    expires: new Date(Date.now() + (60 * 60 * 1000)), // 1 hour from login
    secure: false, // set to true if your using https
    httpOnly: true,
  });
};
module.exports = generateToken;