const { request, response } = require('express');
const propertyModel = require('../../models/user/propertyModel');

const uploadProperty = (request, response) => {
    propertyModel.uploadProperty(request, (err, result) => {
        if (err) {
            response.status(400).send(err);
            // response.status(400).json({message: err, data: request.body});
        } else {
            response.status(200).json({message: 'Property Uploaded Successfully', insertId: result});
        }
    });
}

const propertiesList = (request, response) => {
    propertyModel.propertiesList(request, (err, result) => {
        if (err) {
            response.status(400).send(err);
        } else {
            response.status(200).json({data: result});
        }
    });
}

const addToShortList = (request, response) => {
    propertyModel.addToShortList(request, (err, result) => {
        if (err) {
            response.status(400).send(err);
        } else {
            response.status(200).json({message: result});
        }
    });
}

const removeFromShortList = (request, response) => {
    propertyModel.removeFromShortList(request, (err, result) => {
        if (err) {
            response.status(400).send(err);
        } else {
            response.status(200).json({message: result});
        }
    });
}

const addToRecentlyViewed = (request, response) => {
    propertyModel.addToRecentlyViewed(request, (err, result) => {
        if (err) {
            response.status(400).send(err);
        } else {
            response.status(200).json({message: result});
        }
    });
}

module.exports = {
    uploadProperty,
    propertiesList,
    addToShortList,
    removeFromShortList,
    addToRecentlyViewed
}