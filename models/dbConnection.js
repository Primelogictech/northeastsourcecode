var mysql = require('mysql');

var db = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "real_estate",
});

db.connect(function(error) {
  if (error) console.log('Database connection refused');
});

db.on('error', function(err) {
  if(err.code === 'PROTOCOL_CONNECTION_LOST') {
    console.log('Database connection lost. Trying to reconnect...');
    reConnect();
  } else {
    console.log('Database connection error');
  }
});

function reConnect() {
  var db = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "real_estate",
  });

  db.connect(function(error) {
    if (error) console.log('Database connection refused');
  });
}

// var db  = mysql.createPool({
//   connectionLimit : 10,
//   host: "localhost",
//   user: "root",
//   password: "",
//   database: "real_estate",
// });

// db.getConnection(function(error) {
//   if (error) console.log('Database connection refused');
// });

module.exports = db;