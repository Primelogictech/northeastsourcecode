const { request, response } = require('express');
const db = require('../dbConnection');
var bcrypt = require('bcrypt');
var path = require('path');
const { extname } = require('path');
const fs = require('fs');

const getProperties = (request, response) => {
    const { property_id, location, latitude, longitude, pincode, property_type_id, property_sub_type_id, available_for, max_price, bhk_type, property_status_id, property_age } = request.body;
    if (location && latitude && longitude) {
        var sql = `SELECT p.*, u.name, u.mobile_number, pt.name as property_type, pst.name as property_sub_type, psv.name as property_status_value, GROUP_CONCAT(amenities.name) AS amenities, ( 3959 * acos (cos ( radians(${latitude}) ) * cos( radians( p.latitude ) ) * cos( radians( p.longitude ) - radians(${longitude}) ) + sin ( radians(${latitude}) ) * sin( radians( p.latitude )))) AS distance FROM properties AS p LEFT JOIN users AS u ON p.user_id = u.id LEFT JOIN property_types AS pt ON p.property_type_id = pt.id LEFT JOIN property_sub_types AS pst ON p.property_sub_type_id = pst.id LEFT JOIN property_status_values AS psv ON p.property_status_id = psv.id LEFT JOIN amenities ON FIND_IN_SET(amenities.id, p.available_amenities) WHERE`;
    } else {
        var sql = `SELECT p.*, u.name, u.mobile_number, pt.name as property_type, pst.name as property_sub_type, psv.name as property_status_value, GROUP_CONCAT(amenities.name) AS amenities FROM properties AS p LEFT JOIN users AS u ON p.user_id = u.id LEFT JOIN property_types AS pt ON p.property_type_id = pt.id LEFT JOIN property_sub_types AS pst ON p.property_sub_type_id = pst.id LEFT JOIN property_status_values AS psv ON p.property_status_id = psv.id LEFT JOIN amenities ON FIND_IN_SET(amenities.id, p.available_amenities) WHERE`;
    }
    if (property_id) { // for near by properties in property details page
        sql += ` p.id != '${property_id}' AND`;
    }
    if (property_type_id) {
        sql += ` p.property_type_id = '${property_type_id}' AND`;
    }
    if (property_sub_type_id) {
        sql += ` p.property_sub_type_id = '${property_sub_type_id}' AND`;
    }
    if (available_for) {
        sql += ` p.available_for = '${available_for}' AND`;
    }
    if (max_price) {
        sql += ` p.price <= '${max_price}' AND p.price_range_to <= '${max_price}' AND`;
    }
    if (bhk_type) {
        sql += ` p.bhk_type = '${bhk_type}' AND`;
    }
    if (property_status_id) {
        sql += ` p.property_status_id = '${property_status_id}' AND`;
    }
    if (property_age) {
        sql += ` p.property_age <= '${property_age}' AND`;
    }
    if (pincode) {
        sql += ` p.pincode = '${pincode}' AND`;
    }
    sql += ` p.status = 1 GROUP BY p.id`;
    if (location && latitude && longitude) {
        sql += ` HAVING distance <= 30 ORDER BY distance ASC`;
    } else {
        sql += ` ORDER BY p.id DESC`;
    }
    db.query(sql, (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const login = (request, response) => {
    const { email_or_mobile_number, password } = request.body;
    db.query("SELECT id, email, mobile_number, password, is_service_seller FROM users WHERE email = ? OR mobile_number = ? AND status = 1", [email_or_mobile_number, email_or_mobile_number], (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            if (result.length > 0) {
                hash = (result[0].password);
                if (bcrypt.compareSync(password, hash)) {
                    response(null, result);
                } else {
                    response('Incorrect Password');
                }
            } else {
                response('No User Found');
            }
        }
    });
}

const register = (request, response) => {
    const { name, email, mobile_number, password, otp } = request.body;
    const hashedPassword = bcrypt.hashSync(password, 10);
    db.query("SELECT * FROM otp WHERE mobile_number = ? AND otp = ?", [mobile_number, otp], (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            if (result.length > 0) {
                db.query("INSERT INTO users (name, email, mobile_number, password, role) VALUES (?, ?, ?, ?, ?)", [name, email, mobile_number, hashedPassword, 'User'], (error, result) => {
                    if (error) {
                        response('Database connection error');
                    } else {
                        response(null, 'Registration Successful');
                    }
                });
            } else {
                response('Incorrect OTP');
            }
        }
    });
}

const sendOTP = (request, response) => {
    const { mobile_number } = request.body;
    var otp = '12345';
    db.query("SELECT * FROM otp WHERE mobile_number = ?", [mobile_number], (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            if (result.length > 0) {
                db.query("UPDATE otp SET otp = ? WHERE mobile_number = ?", [otp, mobile_number], (error, result) => {
                    if (error) {
                        response('Database connection error');
                    } else {
                        response(null, 'OTP Sent');
                    }
                });
            } else {
                db.query("INSERT INTO otp (mobile_number, otp) VALUES (?, ?)", [mobile_number, otp], (error, result) => {
                    if (error) {
                        response('Database connection error');
                    } else {
                        response(null, 'OTP Sent');
                    }
                });
            }
        }
    });
}

const servicesList = (request, response) => {
    const { keyword } = request.body;
    var sql = `SELECT * FROM services where status = 1`;
    if (keyword) {
        sql += ` AND name LIKE N'%${keyword}%'`;
    }
    sql += ` ORDER BY priority_order`;
    db.query(sql,  (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const uploadServiceSeller = (request, response) => {
    var services_offered;
    const { id, user_id, name, email, mobile_number, password, business_name, business_experience, address, tags, about, opening_time, closing_time, images_edit } = request.body;
    if (request.body.services_offered) {
        if (Array.isArray(request.body.services_offered)) {
            services_offered = request.body.services_offered.join(',');
        } else {
            services_offered = request.body.services_offered;
        }
    } else {
        return response('Services offered are missing');
    }
    if (user_id) {
        db.query("SELECT id FROM users WHERE id = ?", [user_id], (error, result) => {
            if (error) {
                response('Database connection error');
            } else {
                if (result.length > 0) {
                    if (business_name && business_experience && address && tags && about && opening_time && closing_time && services_offered) {
                        
                        if (request.files && request.files.images) {
                            let sampleFile;
                            let uploadPath;
                            let imagesArray = [];
                            if (id) {
                                db.query('SELECT images FROM service_sellers WHERE id = ?', [id], (error, result) => {
                                    if (error) {
                                        response('Database connection error');
                                    } else {
                                        if (result[0].images) {
                                            var imageArray = result[0].images.split(',');
                                            imageArray.forEach(element => {
                                                var image_path = path.join(__dirname, '../../uploads/') + element;
                                                fs.unlink(image_path, (err) => {
                                                    if (err) {
                                                        response(err);
                                                    }
                                                });
                                            });
                                        }
                                    }
                                });
                            }
                            if (request.files.images.length > 1) {
                                request.files.images.forEach(element => {
                                    var timestamp = Date.now();
                                    var randomString = ("" + Math.random()).substring(2, 8);
                                    var fileName = timestamp + randomString + path.extname(element.name);
                                    uploadPath = path.join(__dirname, '../../uploads/') + fileName;
                                    element.mv(uploadPath);
                                    imagesArray.push(fileName);
                                });
                            } else {
                                sampleFile = request.files.images;
                                var timestamp = Date.now();
                                var randomString = ("" + Math.random()).substring(2, 8);
                                var fileName = timestamp + randomString + path.extname(sampleFile.name);
                                uploadPath = path.join(__dirname, '../../uploads/') + fileName;
                                sampleFile.mv(uploadPath);
                                imagesArray.push(fileName);
                            }
                            var images = imagesArray.join(',');
                        } else {
                            if (id) {
                                images = images_edit;
                            } else {
                                return response('Images missing');
                            }
                        }
                        if (id) {
                            var sql = `UPDATE service_sellers SET user_id = ?, business_name = ?, business_experience = ?, address = ?, tags = ?, about = ?, images = ?, opening_time = ?, closing_time = ?, services_offered = ? WHERE id = ${id}`;
                        } else {
                            var sql = "INSERT INTO service_sellers (user_id, business_name, business_experience, address, tags, about, images, opening_time, closing_time, services_offered) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                        }
                        db.query(sql, [user_id, business_name, business_experience, address, tags, about, images, opening_time, closing_time, services_offered], (error, result) => {
                            if (error) {
                                response('Database connection error');
                            } else {
                                if (id) {
                                    response(null, result.affectedRows);
                                } else {
                                    if (result.insertId) {
                                        db.query('UPDATE users SET is_service_seller = 1 WHERE id = ?', [user_id], (error, result) => {
                                            if (error) {
                                                response('Database connection error');
                                            } else {
                                                response(null, user_id);
                                            }
                                        });
                                    } else {
                                        response('Something went wrong');
                                    }
                                }
                            }
                        });
                    } else {
                        response('Please input all fields');
                    }
                } else {
                    response('No User Found');
                }
            }
        });
    } else {
        if (name && email && mobile_number && password && business_name && business_experience && address && tags && about && opening_time && closing_time && services_offered) {
            const hashedPassword = bcrypt.hashSync(password, 10);
            var sql = "INSERT INTO users (name, email, mobile_number, password, role, is_service_seller) VALUES (?, ?, ?, ?, ?, ?)";
            db.query(sql, [name, email, mobile_number, hashedPassword, 'Service Seller', '1'], (error, result) => {
                if (error) {
                    response('Database connection error');
                } else {
                    if (result.insertId) {
                        const userId = result.insertId;
                        let sampleFile;
                        let uploadPath;
                        let imagesArray = [];
                        if (request.files.images.length > 1) {
                            request.files.images.forEach(element => {
                                var timestamp = Date.now();
                                var randomString = ("" + Math.random()).substring(2, 8);
                                var fileName = timestamp + randomString + path.extname(element.name);
                                uploadPath = path.join(__dirname, '../../uploads/') + fileName;
                                element.mv(uploadPath);
                                imagesArray.push(fileName);
                            });
                        } else {
                            sampleFile = request.files.images;
                            var timestamp = Date.now();
                            var randomString = ("" + Math.random()).substring(2, 8);
                            var fileName = timestamp + randomString + path.extname(sampleFile.name);
                            uploadPath = path.join(__dirname, '../../uploads/') + fileName;
                            sampleFile.mv(uploadPath);
                            imagesArray.push(fileName);
                        }
                        const images = imagesArray.join(',');
                        db.query("INSERT INTO service_sellers (user_id, business_name, business_experience, address, tags, about, images, opening_time, closing_time, services_offered) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [userId, business_name, business_experience, address, tags, about, images, opening_time, closing_time, services_offered], (error, result) => {
                            if (error) {
                                response('Database connection error');
                            } else {
                                response(null, userId);
                            }
                        });
                    } else {
                        response('Something went wrong while creating service seller');
                    }
                }
            });
        } else {
            response('Please input all fields');
        }
    }
}

const getUserData = (request, response) => {
    const id = request.session.userId;
    if (id) {
        db.query("SELECT id, name, email, mobile_number, mobile_number_visibility, alt_mobile_number, alt_mobile_number_visibility, about FROM users WHERE id = ?", [id], (error, result) => {
            if (error) {
                response('Database connection error');
            } else {
                response(null, result);
            }
        });
    } else {
        response('Something went wrong. Please login to try again.');
    }
}

const getPropertyTypes = (request, response) => {
    db.query("SELECT * FROM property_types WHERE status = 1 ORDER BY priority_order ASC", (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const getPropertySubTypes = (request, response) => {
    db.query("SELECT * FROM property_sub_types WHERE status = 1 ORDER BY priority_order ASC", (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const getPropertyStatusValues = (request, response) => {
    db.query("SELECT * FROM property_status_values WHERE status = 1 ORDER BY priority_order ASC", (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const getAmenities = (request, response) => {
    db.query("SELECT * FROM amenities WHERE status = 1 ORDER BY priority_order ASC", (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const getTags = (request, response) => {
    db.query("SELECT * FROM tags WHERE status = 1 ORDER BY priority_order ASC", (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const uploadProperty = (request, response) => {
    var available_amenities;
    const { id, user_id, mobile_number_visibility, alt_mobile_number, alt_mobile_number_visibility, about, seller_type, available_for, property_title, property_type_id, property_sub_type_id, bhk_type, gated_security, floor, total_floors, property_age, facing, property_size, property_size_type, society_name, price_type, price, price_range_to, property_status_id, property_description, address_line_1, latitude, longitude, address_line_2, city, pincode, state, country, floor_plan_edit, property_images_edit } = request.body;
    if (request.body.available_amenities) {
        if (Array.isArray(request.body.available_amenities)) {
            available_amenities = request.body.available_amenities.join(',');
        } else {
            available_amenities = request.body.available_amenities;
        }
    } else {
        return response('Amenities Missing');
    }
    
    if (user_id && alt_mobile_number && about && seller_type && available_for && property_title && property_type_id && property_sub_type_id && bhk_type && gated_security && floor && total_floors && property_age && facing && property_size && property_size_type && society_name && price_type && price && property_status_id && property_description && address_line_1 && latitude && longitude && city && pincode && state && country && available_amenities) {
        db.query("SELECT id FROM users WHERE id = ?", [user_id], (error, result) => {
            if (error) {
                response('User Not Found');
            } else {
                if (result.length > 0) {
                    if (request.files) {
                        var floor_plan;
                        var property_images;
                        if (request.files.floor_plan) {
                            let floorPlanImage;
                            let floorPlanImagePath;
                            let floorPlanImagesArray = [];
                            if (id) {
                                db.query('SELECT floor_plan FROM properties WHERE id = ?', [id], (error, result) => {
                                    if (error) {
                                        response('Database connection error');
                                    } else {
                                        if (result[0].floor_plan) {
                                            var floorPlanArray = result[0].floor_plan.split(',');
                                            floorPlanArray.forEach(element => {
                                                var floor_plan_image_path = path.join(__dirname, '../../uploads/') + element;
                                                fs.unlink(floor_plan_image_path, (err) => {
                                                    if (err) {
                                                        response(err);
                                                    }
                                                });
                                            });
                                        }
                                    }
                                });
                            }
                            if (request.files.floor_plan.length > 1) {
                                request.files.floor_plan.forEach(element => {
                                    var timestamp = Date.now();
                                    var randomString = ("" + Math.random()).substring(2, 8);
                                    var floorPlanImageName = timestamp + randomString + path.extname(element.name);
                                    floorPlanImagePath = path.join(__dirname, '../../uploads/') + floorPlanImageName;
                                    element.mv(floorPlanImagePath);
                                    floorPlanImagesArray.push(floorPlanImageName);
                                });
                            } else {
                                floorPlanImage = request.files.floor_plan;
                                var timestamp = Date.now();
                                var randomString = ("" + Math.random()).substring(2, 8);
                                var floorPlanImageName = timestamp + randomString + path.extname(floorPlanImage.name);
                                floorPlanImagePath = path.join(__dirname, '../../uploads/') + floorPlanImageName;
                                floorPlanImage.mv(floorPlanImagePath);
                                floorPlanImagesArray.push(floorPlanImageName);
                            }
                            floor_plan = floorPlanImagesArray.join(',');
                        } else {
                            if (id) {
                                floor_plan = floor_plan_edit;
                            } else {
                                return response('Floor Plan is missing');
                            }
                        }
                        
                        if (request.files.property_images) {
                            let propertyImage;
                            let propertyImagePath;
                            let propertyImagesArray = [];
                            if (id) {
                                db.query('SELECT property_images FROM properties WHERE id = ?', [id], (error, result) => {
                                    if (error) {
                                        response('Database connection error');
                                    } else {
                                        
                                        if (result[0].property_images) {
                                            var propertyImagesArray = result[0].property_images.split(',');
                                            propertyImagesArray.forEach(element => {
                                                var property_image_path = path.join(__dirname, '../../uploads/') + element;
                                                fs.unlink(property_image_path, (err) => {
                                                    if (err) {
                                                        response(err);
                                                    }
                                                });
                                            });
                                        }
                                    }
                                });
                            }
                            if (request.files.property_images.length > 1) {
                                request.files.property_images.forEach(element => {
                                    var timestamp = Date.now();
                                    var randomString = ("" + Math.random()).substring(2, 8);
                                    var propertyImageName = timestamp + randomString + path.extname(element.name);
                                    propertyImagePath = path.join(__dirname, '../../uploads/') + propertyImageName;
                                    element.mv(propertyImagePath);
                                    propertyImagesArray.push(propertyImageName);
                                });
                            } else {
                                propertyImage = request.files.property_images;
                                var timestamp = Date.now();
                                var randomString = ("" + Math.random()).substring(2, 8);
                                var propertyImageName = timestamp + randomString + path.extname(propertyImage.name);
                                propertyImagePath = path.join(__dirname, '../../uploads/') + propertyImageName;
                                propertyImage.mv(propertyImagePath);
                                propertyImagesArray.push(propertyImageName);
                            }
                            property_images = propertyImagesArray.join(',');
                        } else {
                            if (id) {
                                property_images = property_images_edit;
                            } else {
                                return response('Property Images are missing');
                            }
                        }
                    } else {
                        if (id) {
                            var floor_plan = floor_plan_edit;
                            var property_images = property_images_edit;
                        } else {
                            return response('Floor Plan or Property Images are missing');
                        }
                    }

                    if (id) {
                        var sql = `UPDATE properties SET user_id = ?, seller_type = ?, available_for = ?, property_title = ?, property_type_id = ?, property_sub_type_id = ?, bhk_type = ?, gated_security = ?, floor = ?, total_floors = ?, property_age = ?, facing = ?, property_size = ?, property_size_type = ?, society_name = ?, price_type = ?, price = ?, price_range_to = ?, property_status_id = ?, property_description = ?, floor_plan = ?, property_images = ?, address_line_1 = ?, latitude = ?, longitude = ?, address_line_2 = ?, city = ?, pincode = ?, state = ?, country = ?, available_amenities = ? WHERE id = ${id}`;
                    } else {
                        var sql = "INSERT INTO properties (user_id, seller_type, available_for, property_title, property_type_id, property_sub_type_id, bhk_type, gated_security, floor, total_floors, property_age, facing, property_size, property_size_type, society_name, price_type, price, price_range_to, property_status_id, property_description, floor_plan, property_images, address_line_1, latitude, longitude, address_line_2, city, pincode, state, country, available_amenities) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                    }

                    db.query(sql, [user_id, seller_type, available_for, property_title, property_type_id, property_sub_type_id, bhk_type, gated_security, floor, total_floors, property_age, facing, property_size, property_size_type, society_name, price_type, price, price_range_to, property_status_id, property_description, floor_plan, property_images, address_line_1, latitude, longitude, address_line_2, city, pincode, state, country, available_amenities], (error, result) => {
                        if (error) {
                            response('Property upload failed');
                        } else {
                            if (result) {
                                db.query('UPDATE users SET mobile_number_visibility = ?, alt_mobile_number = ?, alt_mobile_number_visibility = ?, about = ?, is_seller = 1 WHERE id = ?', [mobile_number_visibility, alt_mobile_number, alt_mobile_number_visibility, about, user_id], (error, result) => {
                                    if (error) {
                                        response('Property uploaded. User data upload failed');
                                    } else {
                                        response(null, 'Property uploaded successfully');
                                    }
                                });
                            } else {
                                response('Something went wrong');
                            }
                        }
                    });
                } else {
                    response('No User Found');
                }
            }
        });
    } else {
        response('Please input all fields');
    }
}

const getPropertyData = (request, response) => {
    const id = parseInt(request.params.id);
    if (id) {
        db.query("SELECT p.*, u.name, u.mobile_number, u.about, u.created_at, pt.name as property_type, pst.name as property_sub_type, GROUP_CONCAT(amenities.name) AS amenities FROM properties AS p JOIN users AS u ON p.user_id = u.id LEFT JOIN property_types AS pt ON p.property_type_id = pt.id LEFT JOIN property_sub_types AS pst ON p.property_sub_type_id = pst.id LEFT JOIN amenities ON FIND_IN_SET(amenities.id, p.available_amenities) WHERE p.id = ?", [id], (error, result) => {
            if (error) {
                response('Database connection error');
            } else {
                response(null, result);
            }
        });
    } else {
        response('Property ID Missing');
    }
}

const getServices = (request, response) => {
    db.query("SELECT * FROM services WHERE status = 1", (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const getServiceSellers = (request, response) => {
    const userId = request.session.userId;
    var service_id = parseInt(request.params.id);
    var sql = `SELECT s.*, u.mobile_number, u.status, t.name AS tag_name FROM service_sellers as s LEFT JOIN users as u ON s.user_id = u.id LEFT JOIN tags as t ON s.tags = t.id LEFT JOIN services ON FIND_IN_SET(services.id, s.services_offered) WHERE u.status = 1 AND FIND_IN_SET(${service_id}, s.services_offered)`;
    if (userId) {
        sql += ` AND u.id != ${userId}`;
    }
    sql += ` GROUP BY s.id`;
    db.query(sql, (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const getServiceSellerDetails = (request, response) => {
    var id = parseInt(request.params.id);
    db.query(`SELECT s.*, u.name, u.mobile_number, u.status, GROUP_CONCAT(services.name) AS services FROM service_sellers as s LEFT JOIN users as u ON s.user_id = u.id LEFT JOIN services ON FIND_IN_SET(services.id, s.services_offered) WHERE u.status = 1 AND s.id = ${id}`, (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const getServiceSellerDetailsByUserId = (request, response) => {
    var id = parseInt(request.params.id);
    db.query(`SELECT s.*, u.name, u.mobile_number, u.status, GROUP_CONCAT(services.name) AS services FROM service_sellers as s LEFT JOIN users as u ON s.user_id = u.id LEFT JOIN services ON FIND_IN_SET(services.id, s.services_offered) WHERE u.status = 1 AND u.id = ${id}`, (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const getUserShortListedProperties = (request, response) => {
    const userId = request.session.userId;
    db.query(`SELECT * FROM short_listed_properties where user_id = ${userId}`, (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const getFeaturedProperties = (request, response) => {
    var sql = `SELECT p.*, u.name, u.mobile_number FROM properties AS p LEFT JOIN users AS u ON p.user_id = u.id WHERE p.is_featured = 1 ORDER BY p.id DESC LIMIT 8`;
    db.query(sql, (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const getPropertiesByPropertyType = (request, response) => {
    const { property_type_id  } = request.body;
    var sql = `SELECT p.*, u.name, u.mobile_number FROM properties AS p LEFT JOIN users AS u ON p.user_id = u.id WHERE p.property_type_id = ${property_type_id} AND p.status = 1 ORDER BY p.id DESC LIMIT 8`;
    db.query(sql, (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const getShortListedProperties = (request, response) => {
    const { user_id  } = request.body;
    var sql = `SELECT sp.property_id, p.*, u.name, u.mobile_number FROM short_listed_properties AS sp LEFT JOIN properties AS p ON sp.property_id = p.id LEFT JOIN users AS u ON p.user_id = u.id WHERE sp.user_id = ${user_id} AND p.status = 1 ORDER BY sp.id DESC LIMIT 8`;
    db.query(sql, (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const getAllShortListedProperties = (request, response) => {
    const userId = request.session.userId;
    var sql = `SELECT sp.property_id, p.*, psv.name as property_status_value, u.name, u.mobile_number FROM short_listed_properties AS sp LEFT JOIN properties AS p ON sp.property_id = p.id LEFT JOIN property_status_values AS psv ON p.property_status_id = psv.id LEFT JOIN users AS u ON p.user_id = u.id WHERE sp.user_id = ${userId} AND p.status = 1 ORDER BY sp.id DESC`;
    db.query(sql, (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const getRecentlyViewedProperties = (request, response) => {
    const { user_id  } = request.body;
    var sql = `SELECT rp.property_id, p.*, u.name, u.mobile_number FROM recently_viewed_properties AS rp LEFT JOIN properties AS p ON rp.property_id = p.id LEFT JOIN users AS u ON p.user_id = u.id WHERE rp.user_id = ${user_id} AND p.status = 1 ORDER BY rp.id DESC LIMIT 8`;
    db.query(sql, (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const getPropertySubTypesByPropertyType = (request, response) => {
    const { property_type_id  } = request.body;
    var sql = `SELECT * FROM property_sub_types WHERE property_type_id = ${property_type_id} AND status = 1`;
    db.query(sql, (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const getUserUploadedProperties = (request, response) => {
    const userId = request.session.userId;
    if (userId) {
        var sql = `SELECT p.*, psv.name as property_status_value, COALESCE(r.count, 0) AS total_views FROM properties AS p LEFT JOIN property_status_values AS psv ON p.property_status_id = psv.id LEFT OUTER JOIN (SELECT property_id, count(*) count FROM recently_viewed_properties GROUP BY property_id) r ON p.id = r.property_id WHERE user_id = ${userId}`;
        db.query(sql, (error, result) => {
            if (error) {
                response('Database connection error');
            } else {
                response(null, result);
            }
        });
    } else {
        response('Something went wrong. Please login and try again');
    }
}

const editProfile = (request, response) => {
    const userId = request.session.userId;
    db.query(`SELECT id, name, email, mobile_number, profile_picture FROM users WHERE id = ${userId}`, (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const uploadUserData = (request, response) => {
    const { id, name, email, mobile_number } = request.body;
    db.query('UPDATE users SET name = ?, email = ?, mobile_number = ? WHERE id = ?', [name, email, mobile_number, id], (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const checkEmailUserProfileEdit = (request, response) => {
    const { id, email } = request.body;
    db.query("SELECT email FROM users WHERE email = ? AND id != ?", [email, id], (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const checkMobileNumberUserProfileEdit = (request, response) => {
    const { id, mobile_number} = request.body;
    db.query("SELECT mobile_number FROM users WHERE mobile_number = ? AND id != ?", [mobile_number, id], (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const getMaxPriceInProperties = (request, response) => {
    db.query("SELECT MAX(price) AS from_price, MAX(price_range_to) AS to_price FROM `properties` WHERE status = 1", (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const getServiceBookingsBySellerId = (request, response) => {
    var seller_id = request.session.userId;
    var sql = `SELECT sb.*, u.name, u.mobile_number, s.name AS service_name, s.image AS service_image FROM service_bookings AS sb LEFT JOIN users AS u ON sb.user_id = u.id LEFT JOIN services AS s ON sb.service_id = s.id`;
    if (seller_id) {
        sql += ` WHERE sb.service_seller_id = ${seller_id}`;
    }
    db.query(sql,  (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const changeServiceBookingStatus = (request, response) => {
    const id = parseInt(request.params.id);
    const status = parseInt(request.params.status);
    db.query('UPDATE service_bookings SET status = ? WHERE id = ?', [status, id], (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const getPincodesOfProperties = (request, response) => {
    var sql = `SELECT DISTINCT pincode FROM properties`;
    db.query(sql, (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const uploadUserProfilePicture = (request, response) => {
    const { user_id } = request.body;
    if (request.files && request.files.profile_picture) {
        let sampleFile;
        let uploadPath;

        db.query('SELECT profile_picture FROM users WHERE id = ?', [user_id], (error, result) => {
            if (error) {
                response('Database connection error');
            } else {
                if (result[0].profile_picture) {
                    var image_path = path.join(__dirname, '../../uploads/') + result[0].profile_picture;
                    fs.unlink(image_path, (err) => {
                        if (err) {
                            return response('Error deleting old profile picture');
                        }
                    });
                }
            }
        });
        
        sampleFile = request.files.profile_picture;
        var timestamp = Date.now();
        var randomString = ("" + Math.random()).substring(2, 8);
        var fileName = timestamp + randomString + path.extname(sampleFile.name);
        uploadPath = path.join(__dirname, '../../uploads/') + fileName;
        sampleFile.mv(uploadPath);
    }
    db.query('UPDATE users SET profile_picture = ? WHERE id = ?', [fileName, user_id], (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

module.exports = {
    getProperties,
    login,
    register,
    sendOTP,
    servicesList,
    uploadServiceSeller,
    getUserData,
    getPropertyTypes,
    getPropertySubTypes,
    getPropertyStatusValues,
    getAmenities,
    getTags,
    uploadProperty,
    getPropertyData,
    getServices,
    getServiceSellers,
    getServiceSellerDetails,
    getServiceSellerDetailsByUserId,
    getUserShortListedProperties,
    getFeaturedProperties,
    getPropertiesByPropertyType,
    getShortListedProperties,
    getAllShortListedProperties,
    getRecentlyViewedProperties,
    getPropertySubTypesByPropertyType,
    getUserUploadedProperties,
    editProfile,
    uploadUserData,
    checkEmailUserProfileEdit,
    checkMobileNumberUserProfileEdit,
    getMaxPriceInProperties,
    getServiceBookingsBySellerId,
    changeServiceBookingStatus,
    getPincodesOfProperties,
    uploadUserProfilePicture
}