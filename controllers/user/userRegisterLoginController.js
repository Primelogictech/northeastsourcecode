const { request, response } = require('express');
const userRegisterLoginModel = require('../../models/user/userRegisterLoginModel');

const registerUser = (request, response) => {
    userRegisterLoginModel.checkEmail(request, (err, result) => {
        if (err) {
            response.status(400).send(err);
        } else {
            if (result.length > 0) {
                response.status(200).json({message: 'Email Taken'});
            } else {
                userRegisterLoginModel.checkMobileNumber(request, (err, result) => {
                    if (err) {
                        response.status(400).send(err);
                    } else {
                        if (result.length > 0) {
                            response.status(200).json({message: 'Mobile Number Taken'});
                        } else {
                            userRegisterLoginModel.registerUser(request, (err, result) => {
                                if (err) {
                                    response.status(400).send(err);
                                } else {
                                    response.status(200).json({message: 'Registration Successful', userId: result});
                                }
                            });
                        }
                    }
                });
            }
        }
    });
}

const loginUser = (request, response) => {
    userRegisterLoginModel.loginUser(request, (err, result) => {
        if (err) {
            response.status(400).send(err);
        } else {
            response.status(200).json({message: 'Login Successful', userId: result});
        }
    });
}

const updateUser = (request, response) => {
    userRegisterLoginModel.updateUser(request, (err, result) => {
        if (err) {
            response.status(400).send(err);
        } else {
            if (!result.affectedRows) {
                response.status(200).json({message: 'No User Found'});
            } else {
                response.status(200).json({message: 'User Data Updated Successful'});
            }
        }
    });
}

const checkEmail = (request, response) => {
    userRegisterLoginModel.checkEmail(request, (err, result) => {
        if (err) {
            response.status(400).send(err);
        } else {
            if (result.length > 0) {
                response.status(200).json({message: 'Email Taken'});
            } else {
                response.status(200).json({message: 'Not Found'});
            }
        }
    });
}

const checkMobileNumber = (request, response) => {
    userRegisterLoginModel.checkMobileNumber(request, (err, result) => {
        if (err) {
            response.status(400).send(err);
        } else {
            if (result.length > 0) {
                response.status(200).json({message: 'Mobile Number Taken'});
            } else {
                response.status(200).json({message: 'Not Found'});
            }
        }
    });
}

module.exports = {
    registerUser,
    loginUser,
    updateUser,
    checkEmail,
    checkMobileNumber
}