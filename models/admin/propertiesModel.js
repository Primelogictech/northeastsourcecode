const { request, response } = require('express');
const db = require('../dbConnection');
var path = require('path');
const { extname } = require('path');
const fs = require('fs');

const propertiesList = (request, response) => {
    const { keyword, property_type_id, property_sub_type_id } = request.body;
    var sql = `SELECT p.*, pt.name as property_type, pst.name as property_sub_type, GROUP_CONCAT(amenities.name) AS amenities FROM properties AS p LEFT JOIN property_types AS pt ON p.property_type_id = pt.id LEFT JOIN property_sub_types AS pst ON p.property_sub_type_id = pst.id LEFT JOIN amenities ON FIND_IN_SET(amenities.id, p.available_amenities)`;
    if (keyword || property_type_id || property_sub_type_id) {
        sql += ` WHERE`;
    }
    if (keyword) {
        sql += ` p.property_title LIKE N'%${keyword}%' OR p.society_name LIKE N'%${keyword}%' OR p.city LIKE N'%${keyword}%'`;
    }
    if (keyword && property_type_id) {
        sql += ` AND`;
    }
    if (property_type_id) {
        sql += ` p.property_type_id = '${property_type_id}'`;
    }
    if (property_sub_type_id && (keyword || property_type_id)) {
        sql += ` AND`;
    }
    if (property_sub_type_id) {
        sql += ` p.property_sub_type_id = '${property_sub_type_id}'`;
    }
    sql += ` GROUP BY p.id ORDER BY p.id DESC`;
    db.query(sql,  (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const getPropertyDetails = (request, response) => {
    const id = parseInt(request.params.id);
    db.query("SELECT p.*, u.name, u.mobile_number, pt.name as property_type, pst.name as property_sub_type, GROUP_CONCAT(amenities.name) AS amenities FROM properties AS p JOIN users AS u ON p.user_id = u.id LEFT JOIN property_types AS pt ON p.property_type_id = pt.id LEFT JOIN property_sub_types AS pst ON p.property_sub_type_id = pst.id LEFT JOIN amenities ON FIND_IN_SET(amenities.id, p.available_amenities) WHERE p.id = ?", [id], (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const changePropertyStatus = (request, response) => {
    const id = parseInt(request.params.id);
    const status = parseInt(request.params.status);
    db.query('UPDATE properties SET status = ? WHERE id = ?', [status, id], (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const changeFeaturedStatus = (request, response) => {
    const id = parseInt(request.params.id);
    const status = parseInt(request.params.status);
    db.query('UPDATE properties SET is_featured = ? WHERE id = ?', [status, id], (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            response(null, result);
        }
    });
}

const deleteProperty = (request, response) => {
    const id = parseInt(request.params.id);
    db.query('SELECT floor_plan, property_images FROM properties WHERE id = ?', [id], (error, result) => {
        if (error) {
            response('Database connection error');
        } else {
            if (result[0].floor_plan) {
                var floorPlanArray = result[0].floor_plan.split(',');
                floorPlanArray.forEach(element => {
                    var floor_plan_image_path = path.join(__dirname, '../../uploads/') + element;
                    fs.unlink(floor_plan_image_path, (err) => {
                        if (err) {
                            response(err);
                        }
                    });
                });
            }
            
            if (result[0].property_images) {
                var propertyImagesArray = result[0].property_images.split(',');
                propertyImagesArray.forEach(element => {
                    var property_image_path = path.join(__dirname, '../../uploads/') + element;
                    fs.unlink(property_image_path, (err) => {
                        if (err) {
                            response(err);
                        }
                    });
                });
            }
                
            db.query('DELETE FROM properties WHERE id = ?', [id], (error, result) => {
                if (error) {
                    response('Database connection error');
                } else {
                    response(null, result);
                }
            });
        }
    });
}

module.exports = {
    propertiesList,
    getPropertyDetails,
    changePropertyStatus,
    changeFeaturedStatus,
    deleteProperty
}