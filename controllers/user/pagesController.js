const { request, response } = require('express');
const pagesModel = require('../../models/user/pagesModel');
const userRegisterLoginModel = require('../../models/user/userRegisterLoginModel');

const home = (request, response) => {
    pagesModel.getPropertyTypes(request, (err, pt_result) => {
        if (err) {
            response.render('user/index', { error: err });
        } else {
            pagesModel.getPincodesOfProperties(request, (err, result) => {
                if (err) {
                    response.render('user/index', { error: err });
                } else {
                    response.render('user/index', { pincodes: result, property_types: pt_result });
                }
            });
        }
    });
}

const login = (request, response) => {
    pagesModel.login(request, (err, result) => {
        if (err) {
            response.render('user/login', { error: err });
        } else {
            request.session.userId = result[0].id;
            request.session.isUserServiceSeller = result[0].is_service_seller;
            response.redirect('/');
        }
    });
}

const register = (request, response) => {
    userRegisterLoginModel.checkMobileNumber(request, (err, result) => {
        if (err) {
            response.render('user/register', { error: err });
        } else {
            if (result.length > 0) {
                response.render('user/register', { error: "Mobile Number Taken"});
            } else {
                if (request.body.email) {
                    userRegisterLoginModel.checkEmail(request, (err, result) => {
                        if (err) {
                            response.render('user/register', { error: err });
                        } else {
                            if (result.length > 0) {
                                response.render('user/register', { error: "Email Taken"});
                            } else {
                                pagesModel.register(request, (err, result) => {
                                    if (err) {
                                        response.render('user/register', { error: err });
                                    } else {
                                        response.render('user/register', { success: result });
                                    }
                                });
                            }
                        }
                    });
                } else {
                    pagesModel.register(request, (err, result) => {
                        if (err) {
                            response.render('user/register', { error: err });
                        } else {
                            response.render('user/register', { success: result });
                        }
                    });
                }
            }
        }
    });
}

const sendOTP = (request, response) => {
    pagesModel.sendOTP(request, (err, result) => {
        if (err) {
            response.status(400).json({ message: err });
        } else {
            response.status(200).json({ message: result });
        }
    });
}

const registerServiceSeller = (request, response) => {
    pagesModel.servicesList(request, (err, result) => {
        if (err) {
            response.render('user/register_as_partner', { error: err });
        } else {
            pagesModel.getTags(request, (err, tags) => {
                if (err) {
                    response.render('user/register_as_partner', { error: err });
                } else {
                    if (parseInt(request.params.id)) {
                        pagesModel.getServiceSellerDetailsByUserId(request, (err, s_result) => {
                            const error = request.session.error;
                            const success = request.session.success;
                            delete request.session.error;
                            delete request.session.success;
                            if (s_result.length > 0 && s_result[0].id) {
                                response.render('user/register_as_partner', { data: s_result, services: result, tags: tags, error: error, success: success });
                            } else {
                                response.redirect('/register_as_partner');
                            }
                        });
                    } else {
                        const error = request.session.error;
                        const success = request.session.success;
                        delete request.session.error;
                        delete request.session.success;
                        response.render('user/register_as_partner', { services: result, tags: tags, error: error, success: success });
                    }
                }
            });
        }
    });
}

const uploadServiceSeller = (request, response) => {
    const { id, user_id } = request.body;
    if (id) {
        pagesModel.uploadServiceSeller(request, (err, result) => {
            if (err) {
                request.session.error = err;
                response.redirect('/register_as_partner/'+ user_id);
            } else {
                request.session.success = result;
                response.redirect('/register_as_partner/'+ user_id);
            }
        });
    } else if (user_id) {
        pagesModel.uploadServiceSeller(request, (err, result) => {
            if (err) {
                request.session.error = err;
                response.redirect('/register_as_partner');
            } else {
                request.session.success = result;
                response.redirect('/services');
            }
        });
    } else {
        userRegisterLoginModel.checkEmail(request, (err, result) => {
            if (err) {
                request.session.error = err;
                response.redirect('/register_as_partner');
            } else {
                if (result.length > 0) {
                    request.session.error = 'Email Taken';
                    response.redirect('/register_as_partner');
                } else {
                    userRegisterLoginModel.checkMobileNumber(request, (err, result) => {
                        if (err) {
                            request.session.error = err;
                            response.redirect('/register_as_partner');
                        } else {
                            if (result.length > 0) {
                                request.session.error = 'Mobile Number Taken';
                                response.redirect('/register_as_partner');
                            } else {
                                pagesModel.uploadServiceSeller(request, (err, result) => {
                                    if (err) {
                                        request.session.error = err;
                                        response.redirect('/register_as_partner');
                                    } else {
                                        request.session.success = result;
                                        response.redirect('/services');
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });
    }
}

const postProperty = (request, response) => {
    pagesModel.getUserData(request, (err, u_result) => {
        if (err) {
            response.render('user/login', { error: err });
        } else {
            pagesModel.getPropertyTypes(request, (err, pt_result) => {
                if (err) {
                    response.render('user/post_property', { user_data: u_result, error: err });
                } else {
                    pagesModel.getPropertyStatusValues(request, (err, psv_result) => {
                        if (err) {
                            response.render('user/post_property', { user_data: u_result, error: err });
                        } else {
                            pagesModel.getAmenities(request, (err, a_result) => {
                                if (err) {
                                    response.render('user/post_property', { user_data: u_result, error: err });
                                } else {
                                    if (parseInt(request.params.id)) {
                                        pagesModel.getPropertyData(request, (err, result) => {
                                            if (err) {
                                                response.render('user/post_property', { user_data: u_result, error: err });
                                            } else {
                                                var error = request.session.error;
                                                var success = request.session.success;
                                                delete request.session.error;
                                                delete request.session.success;
                                                response.render('user/post_property', { data: result, user_data: u_result, property_types: pt_result, property_status: psv_result, amenities: a_result, error: error, success: success });
                                            }
                                        });
                                    } else {
                                        var error = request.session.error;
                                        var success = request.session.success;
                                        delete request.session.error;
                                        delete request.session.success;
                                        response.render('user/post_property', { user_data: u_result, property_types: pt_result, property_status: psv_result, amenities: a_result, error: error, success: success });
                                    }
                                    
                                }
                            });
                        }
                    });
                }
            });
        }
    });
}

const uploadProperty = (request, response) => {
    pagesModel.uploadProperty(request, (err, result) => {
        if (err) {
            if (request.body.id) {
                request.session.error = err;
                response.redirect('/my_listings');
            } else {
                request.session.error = err;
                response.redirect('/post_property');
            }
            // response.render('user/post_property', { error: err });
        } else {
            if (request.body.id) {
                request.session.success = result;
                response.redirect('/my_listings');
            } else {
                request.session.success = result;
                response.redirect('/post_property');
            }
            // response.render('user/post_property', { success: result });
        }
    });
}

const getPropertyData = (request, response) => {
    pagesModel.getPropertyData(request, (err, result) => {
        if (err) {
            response.render('user/property_details', { error: err });
        } else {
            const userId = request.session.userId;
            if (userId) {
                pagesModel.getUserShortListedProperties(request, (err, prop) => {
                    if (err) {
                        response.render('user/search_property', { error: err });
                    } else {
                        if (prop && prop.length > 0) {
                            response.render('user/property_details', { data: result, short_list: prop });
                        } else {
                            response.render('user/property_details', { data: result });
                        }
                    }
                });
            } else {
                response.render('user/property_details', { data: result });
            }
        }
    });
}

const searchProperty = (request, response) => {
    pagesModel.getPropertyTypes(request, (err, pt_result) => {
        if (err) {
            response.render('user/post_property', { user_data: u_result, error: err });
        } else {
            pagesModel.getPropertyStatusValues(request, (err, psv_result) => {
                if (err) {
                    response.render('user/search_property', { error: err });
                } else {
                    pagesModel.getPincodesOfProperties(request, (err, pin_result) => {
                        if (err) {
                            response.render('user/search_property', { error: err });
                        } else {
                            const userId = request.session.userId;
                            if (userId) {
                                pagesModel.getUserShortListedProperties(request, (err, prop) => {
                                    if (err) {
                                        response.render('user/search_property', { error: err });
                                    } else {
                                        if (prop && prop.length > 0) {
                                            response.render('user/search_property', { property_types: pt_result, property_status: psv_result, pincodes: pin_result, short_list: prop, form_data: request.body });
                                        } else {
                                            response.render('user/search_property', { property_types: pt_result, property_status: psv_result, pincodes: pin_result, form_data: request.body });
                                        }
                                    }
                                });
                            } else {
                                response.render('user/search_property', { property_types: pt_result, property_status: psv_result, pincodes: pin_result, form_data: request.body });
                            }
                        }
                    });
                }
            });
        }
    });
}

const services = (request, response) => {
    pagesModel.getServices(request, (err, result) => {
        if (err) {
            response.render('user/services', { error: err });
        } else {
            const error = request.session.error;
            const success = request.session.success;
            delete request.session.error;
            delete request.session.success;
            response.render('user/services', { data: result, error: error, success: success });
        }
    });
}

const serviceCategory = (request, response) => {
    const service_category = parseInt(request.params.id);
    pagesModel.getServices(request, (err, s_result) => {
        if (err) {
            response.render('user/service_category', { error: err });
        } else {
            pagesModel.getServiceSellers(request, (err, result) => {
                if (err) {
                    response.render('user/service_category', { error: err });
                } else {
                    response.render('user/service_category', { data: result, services: s_result, service_category: service_category });
                }
            });
        }
    });
}

const serviceSellerDetails = (request, response) => {
    const service_id = parseInt(request.params.service_id);
    pagesModel.getServiceSellerDetails(request, (err, result) => {
        if (err) {
            response.render('user/service_seller_details', { error: err });
        } else {
            response.render('user/service_seller_details', { data: result, service_id: service_id });
        }
    });
}

const getFeaturedProperties = (request, response) => {
    pagesModel.getFeaturedProperties(request, (err, result) => {
        if (err) {
            response.render('user/getProperties', { error: err });
        } else {
            response.render('user/getProperties', { data: result });
        }
    });
}

const getPropertiesByPropertyType = (request, response) => {
    pagesModel.getPropertiesByPropertyType(request, (err, result) => {
        if (err) {
            response.render('user/getProperties', { error: err });
        } else {
            response.render('user/getProperties', { data: result });
        }
    });
}

const getShortListedProperties = (request, response) => {
    pagesModel.getShortListedProperties(request, (err, result) => {
        if (err) {
            response.render('user/getProperties', { error: err });
        } else {
            response.render('user/getProperties', { data: result });
        }
    });
}

const getRecentlyViewedProperties = (request, response) => {
    pagesModel.getRecentlyViewedProperties(request, (err, result) => {
        if (err) {
            response.render('user/getProperties', { error: err });
        } else {
            response.render('user/getProperties', { data: result });
        }
    });
}

const getPropertySubTypesByPropertyType = (request, response) => {
    pagesModel.getPropertySubTypesByPropertyType(request, (err, result) => {
        if (err) {
            response.render('user/getPropertySubTypes', { error: err });
        } else {
            response.render('user/getPropertySubTypes', { data: result, property_sub_type_id: request.body.property_sub_type_id });
        }
    });
}

const getUserUploadedProperties = (request, response) => {
    pagesModel.getUserUploadedProperties(request, (err, result) => {
        if (err) {
            response.render('user/my_listings', { error: err });
        } else {
            var error = request.session.error;
            var success = request.session.success;
            delete request.session.error;
            delete request.session.success;
            response.render('user/my_listings', { data: result, error: error, success: success });
        }
    });
}

const editProfile = (request, response) => {
    pagesModel.editProfile(request, (err, result) => {
        if (err) {
            response.render('user/edit_profile', { error: err });
        } else {
            var error = request.session.error;
            var success = request.session.success;
            delete request.session.error;
            delete request.session.success;
            response.render('user/edit_profile', { data: result, error: error, success: success });
        }
    });
}

const uploadUserData = (request, response) => {
    pagesModel.checkEmailUserProfileEdit(request, (err, result) => {
        if (err) {
            request.session.error = err;
            response.redirect('/edit_profile');
        } else {
            if (result.length > 0) {
                request.session.error = "Email Taken";
                response.redirect('/edit_profile');
            } else {
                pagesModel.checkMobileNumberUserProfileEdit(request, (err, result) => {
                    if (err) {
                        request.session.error = err;
                        response.redirect('/edit_profile');
                    } else {
                        if (result.length > 0) {
                            request.session.error = "Mobile Number Taken";
                            response.redirect('/edit_profile');
                        } else {
                            pagesModel.uploadUserData(request, (err, result) => {
                                if (err) {
                                    request.session.error = err;
                                    response.redirect('/edit_profile');
                                } else {
                                    request.session.success = "Profile updated successfully";
                                    response.redirect('/edit_profile');
                                }
                            });
                        }
                    }
                });
            }
        }
    });
}

const getNearByProperties = (request, response) => {
    pagesModel.getProperties(request, (err, result) => {
        if (err) {
            response.render('user/getNearByProperties', { error: err });
        } else {
            response.render('user/getNearByProperties', { data: result });
        }
    });
}

const getMaxPriceInProperties = (request, response) => {
    pagesModel.getMaxPriceInProperties(request, (err, result) => {
        if (err) {
            response.status(400).json({ error: err });
        } else {
            if (result) {
                if (result[0].to_price > result[0].from_price) {
                    max_price = result[0].to_price;
                } else {
                    max_price = result[0].from_price;
                }
                response.status(200).json({ max_price: max_price });
            } else {
                response.status(200).json({ max_price: 0 });
            }
        }
    });
}

const getPropertiesByFilters = (request, response) => {
    pagesModel.getProperties(request, (err, result) => {
        if (err) {
            response.render('user/getPropertiesByFilters', { error: err });
        } else {
            const userId = request.session.userId;
            if (userId) {
                pagesModel.getUserShortListedProperties(request, (err, prop) => {
                    if (err) {
                        response.render('user/search_property', { error: err });
                    } else {
                        if (prop && prop.length > 0) {
                            response.render('user/getPropertiesByFilters', { data: result, short_list: prop });
                        } else {
                            response.render('user/getPropertiesByFilters', { data: result });
                        }
                    }
                });
            } else {
                response.render('user/getPropertiesByFilters', { data: result });
            }
        }
    });
}

const viewBookings = (request, response) => {
    pagesModel.getServiceBookingsBySellerId(request, (err, result) => {
        if (err) {
            response.render('user/viewBookings', { error: err });
        } else {
            response.render('user/viewBookings', { data: result });
        }
    });
}

const changeServiceBookingStatus = (request, response) => {
    pagesModel.changeServiceBookingStatus(request, (err, result) => {
        if (err) {
            response.status(400).json({error: err});
        } else {
            if (!result.affectedRows) {
                response.status(200).json({warning: 'No Service Booking Found'});
            } else {
                response.status(200).json({success: 'Status Changed Successfully'});
            }
        }
    });
}

const shortListedProperties = (request, response) => {
    pagesModel.getAllShortListedProperties(request, (err, result) => {
        if (err) {
            response.render('user/shortListedProperties', { error: err });
        } else {
            response.render('user/shortListedProperties', { data: result });
        }
    });
}

const uploadUserProfilePicture = (request, response) => {
    pagesModel.uploadUserProfilePicture(request, (err, result) => {
        if (err) {
            request.session.error = err;
            response.redirect('/edit_profile');
        } else {
            request.session.success = "Profile Picture Updated Successfully";
            response.redirect('/edit_profile');
        }
    });
}

module.exports = {
    home,
    login,
    register,
    sendOTP,
    registerServiceSeller,
    uploadServiceSeller,
    postProperty,
    uploadProperty,
    getPropertyData,
    searchProperty,
    services,
    serviceCategory,
    serviceSellerDetails,
    getFeaturedProperties,
    getPropertiesByPropertyType,
    getShortListedProperties,
    getRecentlyViewedProperties,
    getPropertySubTypesByPropertyType,
    getUserUploadedProperties,
    editProfile,
    uploadUserData,
    getNearByProperties,
    getMaxPriceInProperties,
    getPropertiesByFilters,
    viewBookings,
    changeServiceBookingStatus,
    shortListedProperties,
    uploadUserProfilePicture
}